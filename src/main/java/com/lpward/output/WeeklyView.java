/*
Copyright (c) 2016 Julius T. Kosan

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

package com.lpward.output;

import java.util.ArrayList;
import java.util.List;
import com.lpward.domain.DaysTimesRoom;

public class WeeklyView {

    private List<DaysTimesRoom> daysTimesRooms = new ArrayList<DaysTimesRoom>();

    public void addDaysTimesRooms(DaysTimesRoom daysTimesRoom) {
	daysTimesRooms.add(daysTimesRoom);
    }

    public String getView(boolean isRoom) {
	String[][] timeSlots = new String[31][5];

	for (DaysTimesRoom daysTimesRoom : daysTimesRooms) {

	    String courseName = daysTimesRoom.getName();
	    
	    if(! isRoom)
	    {
		courseName += "(" + daysTimesRoom.getBuilding() + daysTimesRoom.getRoomNumber() + ")";
	    }

	    int dayPattern = daysTimesRoom.getDaysPattern();
	    int startSlot = (daysTimesRoom.getStartSlot() - 96) / 6; //96 means ignore 12AM-8AM.
	    int slotsLength = daysTimesRoom.getSlotsLength() / 6;
	    int endSlot = startSlot + slotsLength + 1;

	    for (int time = 0; time <= 30; time++) { //todo:tk:should this be <= 30 or < 30?
		int bitMask = 64;

		for (int day = 0; day < 5; day++) {
		    if ((dayPattern & bitMask) > 0) {

			//System.out.println(time + "  " + day);
			if (time >= startSlot && time < endSlot) {
			    if(timeSlots[time][day] == null)
			    {
				timeSlots[time][day] = courseName;
			    }
			    else
			    {
				timeSlots[time][day] += ", " + courseName;
			    }
			}
		    }

		    bitMask = bitMask >> 1;
		}
	    }
	}

	StringBuilder sb = new StringBuilder();

	String[] halfHours = new String[] {

	"8:00am", "8:30am", "9:00am", "9:30am", "10:00am", "10:30am",
		"11:00am", "11:30am", "12:00pm", "12:30pm", "1:00pm", "1:30pm",
		"2:00pm", "2:30pm", "3:00pm", "3:30pm", "4:00pm", "4:30pm",
		"5:00pm", "5:30pm", "6:00pm", "6:30pm", "7:00pm", "7:30pm",
		"8:00pm", "8:30pm", "9:00pm", "9:30pm", "10:00pm", "10:30pm",
		"11:00pm", "11:30pm" };

	sb.append("|Monday|Tuesday|Wednesday|Thursday|Friday|\n");

	for (int time = 0; time <= 30; time++) {

	    sb.append(halfHours[time] + "|");

	    for (int day = 0; day < 5; day++) {

		if (timeSlots[time][day] != null) {
		    sb.append(timeSlots[time][day] + "|");
		} else {
		    sb.append("|");
		}

	    }

	    sb.append("\n");

	}

	return sb.toString();

    }

}
