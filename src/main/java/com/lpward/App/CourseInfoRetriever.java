/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lpward.App;

import com.lpward.input.XML;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.Timer;
import java.util.TimerTask;

/**
 *
 * @author Liam Ward
 */
public class CourseInfoRetriever {
    private String courseInfoURL = "http://integration04.shawnee.edu/StudentScheduleExport/courses.xml";

    public CourseInfoRetriever() throws Exception{
        final Timer timer = new Timer();
        
        refreshData();

        timer.scheduleAtFixedRate(new TimerTask() {
            public void run() {
                try
                {
                        refreshData();
                }
                catch(Exception e)
                {
                        e.printStackTrace();
                        //LOGGER.info("ERROR processing XML file, exception message: " + e.getMessage());
                        //System.err.println(e.getMessage());
                }
            }

        }, 120000, 120000);
        
    }
    
    private void refreshData() throws Exception{
        XML xmlReader = new XML();
		 
        //URL url= new URL("http://localhost/courses_3.xml");
        URL url= new URL(courseInfoURL);
        
        //String xmlFilename = "src/main/resources/com/lpward/persistence/courses.xml";
        String xmlFilename = "./courses.xml";
        //xmlFilename = "";
        downloadFromUrl(url, xmlFilename);
        
        xmlReader.parse(xmlFilename);
    }
    void downloadFromUrl(URL url, String localFilename) throws IOException {
        InputStream is = null;
        FileOutputStream fos = null;

        try {
            URLConnection urlConn = url.openConnection();

            urlConn.setConnectTimeout(20 * 1000);

            is = urlConn.getInputStream();

            fos = new FileOutputStream(localFilename, false);

            byte[] buffer = new byte[8192];
            int len;

            while ((len = is.read(buffer)) > 0) {  
                fos.write(buffer, 0, len);
            }
        } finally {
            try {
                if (is != null) {
                    is.close();
                }
            } finally {
                if (fos != null) {
                    fos.close();
                }
            }
        }
    }
}
