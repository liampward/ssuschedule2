/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lpward.App;

import com.lpward.domain.StudentSchedule;
import com.lpward.input.XML;
import com.lpward.solver.ScheduleSolver;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;
//import org.takes.Response;
//import org.takes.facets.fallback.Fallback;
//import org.takes.facets.fallback.FbChain;
//import org.takes.facets.fallback.FbStatus;
//import org.takes.facets.fallback.RqFallback;
//import org.takes.facets.fallback.TkFallback;
//import org.takes.http.Exit;
//import org.takes.http.FtBasic;
//import org.takes.facets.fork.FkRegex;
//import org.takes.facets.fork.TkFork;
//import org.takes.misc.Opt;
//import org.takes.misc.Opt.Single;
//import org.takes.rs.RsHtml;
//import org.takes.rs.RsText;
import org.takes.facets.fallback.*;
import org.takes.rs.*;
import org.takes.rq.*;
import org.takes.http.*;
import org.takes.misc.*;
import org.takes.*;
import org.takes.facets.fork.*;
import org.takes.misc.Opt.Single;
import org.takes.tk.TkFiles;
import org.takes.tk.TkWithType;

public class App{
    static String localHost = "src/main/resources/";
    //static String coursesXML = "com/lpward/persistence/courses.xml";
    static String coursesXML = "./courses.xml";
    //static String jsonfname = "com/lpward/persistence/classrequests.json";
//    static String generatorHtml = "com/lpward/persistence/html/index.html";
//    static String jsResource = "com/lpward/persistence/generator.js";
//    static String cssResource = "com/lpward/persistence/html";
    
    static String generatorHtml = "./resources/html/index.html";
    static String jsResource = "./resources/generator.js";
    static String cssResource = "./resources/html";
    
//    static String cssResourceHtml = "com/lpward/persistence/html/css/html.css";
//    static String cssResourceBase = "com/lpward/persistence/html/css/base.css";
//    static String cssResourceMain = "com/lpward/persistence/html/css/main.css";
//    
//    static File cssHtml = new File(cssResourceHtml);
    
    public static void main(final String... args) throws Exception{
        CourseInfoRetriever courseRetriever = new CourseInfoRetriever();
        
        XML xmlReader = new XML();
        xmlReader.parse(coursesXML);
        
        //List<StudentSchedule> scheduleList = ScheduleSolver.readClassRequests(xmlReader, jsonfname);
        
        while(true){
            try{
                new FtBasic(
                    new TkFallback(
                        new TkFork(
                            
                            new FkRegex("/generator/generate", new TkGenerateSchedule(xmlReader)),
                            new FkRegex("/", new RsHtml(new FileInputStream(generatorHtml))),
                            new FkRegex("/generator/generator.js", new RsText(new FileInputStream(jsResource))),
                            new FkRegex("/generator/courselist", new TkGetCourseList(xmlReader)),
                            new FkRegex("/css/.+", new TkWithType(
                                                    new TkFiles(cssResource), "text/css"))
                        ),
                        new FbChain(
                            new FbStatus(404, new RsText("Sorry, we cannot find this page!")),
                            new FbStatus(405, new RsText("This method is not allowed.")),
                            new Fallback(){
                                @Override
                                public Opt<Response> route(RqFallback rf) throws IOException {
                                    System.out.println(rf.throwable().getMessage());
                                    return new Single<>(
                                        new RsHtml("Oops!")
                                    );
                                }
                            }
                        )
                    ),
                    80
                ).start(Exit.NEVER);
            } catch(IOException e){
                System.out.println(e);
            }
        }
    }
}
