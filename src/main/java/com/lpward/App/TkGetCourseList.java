
package com.lpward.App;

import com.lpward.input.SemesterSchedule;
import com.lpward.input.XML;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.takes.Request;
import org.takes.Response;
import org.takes.Take;
import org.takes.rs.RsJson;

public class TkGetCourseList implements Take{
    
    private XML semesterSchedule;
    private String xmlSchedule = "./courses.xml";
    //private String xmlSchedule = "src/main/resources/com/lpward/persistence/courses.xml";
    
    public TkGetCourseList(SemesterSchedule semesterSchedule){
        this.semesterSchedule = (XML)semesterSchedule;
    }
    
//    private void updateCourseInfo(){
//        try {
//            semesterSchedule.parse(new InputSource(new URL(courseInfoURL).openStream()));
//        } catch (MalformedURLException ex) {
//            Logger.getLogger(TkGetCourseList.class.getName()).log(Level.SEVERE, null, ex);
//        }
//
//    }
    
    @Override
    public Response act(Request rqst) throws IOException {
        try {
            semesterSchedule.parse(xmlSchedule);
        } catch (Exception ex) {
            Logger.getLogger(TkGetCourseList.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return new RsJson(semesterSchedule);
    }    
}
