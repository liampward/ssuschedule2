
package com.lpward.App;

import com.lpward.domain.StudentSchedule;
import com.lpward.domain.StudentScheduleList;
import com.lpward.domain.course.Section;
import com.lpward.input.SemesterSchedule;
import com.lpward.input.XML;
import com.lpward.solver.AllBestSolutionsRecaller;
import com.lpward.solver.MultiSolutionSolver;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import static org.apache.commons.lang3.ObjectUtils.defaultIfNull;
import org.optaplanner.core.api.solver.Solver;
import org.optaplanner.core.api.solver.SolverFactory;
import org.optaplanner.core.config.SolverConfigContext;
import org.optaplanner.core.config.heuristic.policy.HeuristicConfigPolicy;
import org.optaplanner.core.config.score.director.ScoreDirectorFactoryConfig;
import org.optaplanner.core.config.solver.EnvironmentMode;
import org.optaplanner.core.config.solver.SolverConfig;
import org.optaplanner.core.config.solver.recaller.BestSolutionRecallerConfig;
import org.optaplanner.core.config.solver.termination.TerminationConfig;
import org.optaplanner.core.impl.domain.solution.descriptor.SolutionDescriptor;
import org.optaplanner.core.impl.phase.Phase;
import org.optaplanner.core.impl.score.director.InnerScoreDirectorFactory;
import org.optaplanner.core.impl.solver.random.RandomFactory;
import org.optaplanner.core.impl.solver.recaller.BestSolutionRecaller;
import org.optaplanner.core.impl.solver.scope.DefaultSolverScope;
import org.optaplanner.core.impl.solver.termination.BasicPlumbingTermination;
import org.optaplanner.core.impl.solver.termination.Termination;
import org.takes.Request;
import org.takes.Response;
import org.takes.Take;
import org.takes.facets.flash.RsFlash;
import org.takes.facets.forward.RsForward;
import org.takes.misc.Href;
import org.takes.rq.RqHref;
import org.takes.rs.RsEmpty;
import org.takes.rs.RsJson;


public class TkGenerateSchedule implements Take{
    
    XML semesterSchedule;
    Solver<StudentSchedule> solver;
    
    //This stuff is the BestSolutionRecaller hack. Basically it lets me use the
    //AllBestSolutionsRecaller I built so that I can... recall.. all best solutionss..
    //A lot of it is copied straight from the Optaplanner source.
    static SolverConfig solverConfig = new SolverConfig(){
        @Override
        public <Solution_> Solver<Solution_> buildSolver(SolverConfigContext configContext){
            configContext.validate();
            EnvironmentMode environmentMode_ = determineEnvironmentMode();
            boolean daemon_ = defaultIfNull(daemon, false);

            RandomFactory randomFactory = buildRandomFactory(environmentMode_);
            Integer moveThreadCount_ = resolveMoveThreadCount();
            SolutionDescriptor<Solution_> solutionDescriptor = buildSolutionDescriptor(configContext);
            ScoreDirectorFactoryConfig scoreDirectorFactoryConfig_
                    = scoreDirectorFactoryConfig == null ? new ScoreDirectorFactoryConfig()
                    : scoreDirectorFactoryConfig;
            InnerScoreDirectorFactory<Solution_> scoreDirectorFactory = scoreDirectorFactoryConfig_.buildScoreDirectorFactory(
                    configContext, environmentMode_, solutionDescriptor);
            boolean constraintMatchEnabledPreference = environmentMode_.isAsserted();
            DefaultSolverScope<Solution_> solverScope = new DefaultSolverScope<>();
            solverScope.setScoreDirector(scoreDirectorFactory.buildScoreDirector(true, constraintMatchEnabledPreference));
            
            //This is the part that I changed.
            BestSolutionRecallerConfig allBestSolutionsRecallerConfig = new BestSolutionRecallerConfig(){
                //Made this call my own function so we could return a type of AllBestSolutionsRecaller
                //And not get yelled at for type missmatch.
                @Override
                public <Solution_> BestSolutionRecaller<Solution_> buildBestSolutionRecaller(EnvironmentMode environmentMode){
                    return buildAllBestSolutionsRecaller(environmentMode);
                }
                //This does what it says.
                public <Solution_> AllBestSolutionsRecaller<Solution_> buildAllBestSolutionsRecaller(EnvironmentMode environmentMode){
                    AllBestSolutionsRecaller<Solution_> bestSolutionRecaller = new AllBestSolutionsRecaller<>();
                    if (environmentMode.isNonIntrusiveFullAsserted()) {
                        bestSolutionRecaller.setAssertInitialScoreFromScratch(true);
                        bestSolutionRecaller.setAssertShadowVariablesAreNotStale(true);
                        bestSolutionRecaller.setAssertBestScoreIsUnmodified(true);
                    }
                    return bestSolutionRecaller;
                }
            };
        
            BestSolutionRecaller<Solution_> bestSolutionRecaller = allBestSolutionsRecallerConfig
                    .buildBestSolutionRecaller(environmentMode_);
            HeuristicConfigPolicy configPolicy = new HeuristicConfigPolicy(environmentMode_,
                    moveThreadCount_, moveThreadBufferSize, threadFactoryClass,
                    scoreDirectorFactory);
            TerminationConfig terminationConfig_ = getTerminationConfig() == null ? new TerminationConfig()
                    : getTerminationConfig();
            BasicPlumbingTermination basicPlumbingTermination = new BasicPlumbingTermination(daemon_);
            Termination termination = terminationConfig_.buildTermination(configPolicy, basicPlumbingTermination);
            List<Phase<Solution_>> phaseList = buildPhaseList(configPolicy, bestSolutionRecaller, termination);
            return new MultiSolutionSolver<>(environmentMode_, randomFactory,
                    bestSolutionRecaller, basicPlumbingTermination, termination, phaseList, solverScope);
        }
    };
    
    public TkGenerateSchedule(SemesterSchedule semesterSchedule){
        this.semesterSchedule = (XML)semesterSchedule;
        SolverFactory<StudentSchedule> sf = SolverFactory.createFromXmlResource("com/lpward/solver/studentScheduleSolverConfig.xml");
        solverConfig.inherit(sf.getSolverConfig());
        solver = solverConfig.buildSolver(new SolverConfigContext());
    }
    
    @Override
    public Response act(Request rqst) throws IOException {
        try {
            semesterSchedule.parse("./courses.xml");
        } catch (Exception ex) {
            Logger.getLogger(TkGenerateSchedule.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        //We have to make sure our solutions list is empty, otherwise we'll be reusing solutions.
        ((MultiSolutionSolver)solver).emptySolutionsList();
        
        Href href = new RqHref.Base(rqst).href();
        
        while(rqst.body().available() > 0){
            System.out.println((char)rqst.body().read());
        }
        List<String> courseRequests = new ArrayList<>();
        List<String> excludedSections = new ArrayList<>();
        String startTime, endTime, days, boolString;
        boolean daysOverTimes = true;
        boolean includeOnline = false;
        boolean includeHonors = false;
        boolean includeClosed = false;
        Iterable<String> values;
        
        values = href.param("Course");
        for(String val : values){
            courseRequests.add(val);
            System.out.println(val);
        }
        values = href.param("StartPref");
        startTime = values.iterator().next();
        values = href.param("EndPref");
        endTime = values.iterator().next();
        
        values = href.param("ExcludeSec");
        for(String sec : values){
            excludedSections.add(sec);
        }
        
        values = href.param("Days");
        days = "";
        for(String day : values){
            days += day;
        }System.out.println(days);
        
        values = href.param("TimesDaysPref");
        boolString = values.iterator().next();
        if(boolString.equals("times"))
            daysOverTimes = false;
        
        values = href.param("Online");
        boolString = values.iterator().next();
        if(boolString.equals("true"))
            includeOnline = true;
        
        values = href.param("Honors");
        boolString = values.iterator().next();
        if(boolString.equals("true"))
            includeHonors = true;
        
        values = href.param("Closed");
        boolString = values.iterator().next();
        if(boolString.equals("true"))
            includeClosed = true;
        
        if(courseRequests.isEmpty()){
            return new RsForward(
                new RsFlash("Please select requested classes"),
                "/generate"
            );
        }
        try{
            StudentScheduleList scheduleList = new StudentScheduleList();
            String[] rArray = new String[8];
            courseRequests.toArray(rArray);
            StudentSchedule unsolved = StudentSchedule.CreateStudentSchedule(
                    semesterSchedule, rArray, startTime, endTime, days, daysOverTimes,
                    includeOnline, includeHonors, includeClosed, excludedSections);
            
            StudentSchedule solved = solver.solve(unsolved);
            
            scheduleList.setStudentScheduleList(((MultiSolutionSolver)solver).getBestSolutions());
            if(solver.getBestSolution().getScore().isFeasible() 
                    && ((MultiSolutionSolver)solver).getBestSolutions().isEmpty())
                    scheduleList.add(solved);
            
            for(StudentSchedule ss : (List<StudentSchedule>)((MultiSolutionSolver)solver).getBestSolutions()){
                System.out.println("HARD: " + ss.getScore().getHardScore() + " SOFT: " + ss.getScore().getSoftScore());
            }
            if(((MultiSolutionSolver)solver).getBestSolutions().isEmpty()){
                //System.out.println(solver.getBestSolution().getScore().getHardScore());
                
                System.out.println("FAILED TO PRODUCE SCHEDULE!");
                System.out.println("HARD: " + solver.getBestSolution().getScore().getHardScore() 
                        + " SOFT: " + solver.getBestSolution().getScore().getSoftScore());

                return new RsEmpty();
            }
            System.out.println("SENDING!");
            return new RsJson(scheduleList);
        } catch(Exception e){
            System.out.println(e);
            return new RsJson(new RsEmpty());
        }
    }
    
}
