/*
Copyright (c) 2016 Julius T. Kosan

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package com.lpward.input;

//import de.timefinder.data.IntervalLongImpl;
import com.lpward.domain.DaysTimesRoom;
import com.lpward.domain.course.Section;
import java.awt.Color;
import java.io.File;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import com.lpward.domain.course.Course;
import com.lpward.domain.course.Instructor;
import com.lpward.output.WeeklyView;
import java.io.IOException;
import java.util.Map.Entry;
import java.util.Set;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonBuilderFactory;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import org.takes.rs.RsJson.Source;

public abstract class SemesterSchedule implements Source{
    
    protected DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("M/d/y");

    public static <K, V extends Comparable<? super V>> Map<K, V> sortByValue(Map<K, V> map) {
        return map.entrySet()
                .stream()
                .sorted(Map.Entry.comparingByValue(/*Collections.reverseOrder()*/))
                .collect(Collectors.toMap(
                        Map.Entry::getKey,
                        Map.Entry::getValue,
                        (e1, e2) -> e1,
                        LinkedHashMap::new
                ));
    }

    protected Map<String, Course> courses;

    public SemesterSchedule() {
        courses = new HashMap<String, Course>();
    }

    public Map<String, Course> coursesSubset(String[] coursesToKeep) {

        List<String> keys = new ArrayList(Arrays.asList(coursesToKeep));

        Map<String, Course> subset = keys.stream()
                .filter(courses::containsKey)
                .collect(Collectors.toMap(Function.identity(), courses::get));

        return subset;
    }
    
    public List<Course> coursesSubsetList(String[] coursesToKeep){
        List<String> keys = new ArrayList(Arrays.asList(coursesToKeep));

        List<Course> subset = new ArrayList<>();
        
        for(String key : keys){
            if(courses.containsKey(key))
                subset.add(courses.get(key));
        }
        return subset;
    }

    public Map<String, Course> coursesSubset(String numberStartsWith) {

        Map<String, Course> subset = new HashMap<String, Course>();

        for (Course course : courses.values()) {
            if (course.getNumber().toLowerCase().startsWith(numberStartsWith.toLowerCase())) {
                subset.put(course.getNumber(), course);
            }
        }

        return subset;
    }

    public List<Instructor> getAllInstructors() {

        Map<String, Instructor> instructorsMap = new HashMap<String, Instructor>();

        int noEmailAddressCounter = 0;

        for (Course course : courses.values()) {
            for (Section section : course.getSections()) {

                Instructor instructorOfRecord = section.getInstructorOfRecord();

                String emailAddress = instructorOfRecord.getEmailAddress();

                if (emailAddress != null) {
                    emailAddress = emailAddress.trim();
                }

                if (emailAddress.equals("")) {
                    emailAddress = "none" + noEmailAddressCounter++;
                }

                if (!instructorsMap.containsKey(emailAddress)) {
                    instructorsMap.put(emailAddress, instructorOfRecord);
                }
            }
        }

        List<Instructor> instructorsList = new ArrayList<Instructor>(instructorsMap.values());

        Collections.sort(instructorsList);

        return instructorsList;
    }

    public List<String> getAllRooms(boolean isMathPiper) {

        List<String> roomsList = new ArrayList<String>();

        for (Course course : courses.values()) {
            for (Section section : course.getSections()) {
                for (DaysTimesRoom daysTimesRoom : section.getDaysTimesRoomList()) {

                    String roomName = daysTimesRoom.getBuilding() + daysTimesRoom.getRoomNumber();
                    
                    if(isMathPiper)
                    {
                        roomName = "\"" + roomName + "\"";
                    }

                    if (!roomsList.contains(roomName)) {
                        roomsList.add(roomName);
                    }
                }
            }
        }

        Collections.sort(roomsList);

        return roomsList;
    }
    
    
        public List<DaysTimesRoom> getDaysTimesRoomList(String[] courseAndSectionArray) {

        List<DaysTimesRoom> daysTimesRoomList = new ArrayList<DaysTimesRoom>();

        for (String courseAndSectionString : courseAndSectionArray) {
            String[] courseAndSectionList = courseAndSectionString.split("-");
            Course course = courses.get(courseAndSectionList[0]);
            Section section = course.getSection(courseAndSectionList[1]);
            
            for (DaysTimesRoom daysTimesRoom : section.getDaysTimesRoomList()) {
                daysTimesRoomList.add(daysTimesRoom);
            }
        }

        return daysTimesRoomList;
    }
    
    
    public List<DaysTimesRoom> getDaysTimesRoomList() {

        List<DaysTimesRoom> daysTimesRoomList = new ArrayList<DaysTimesRoom>();

        for (Course course : courses.values()) {
            for (Section section : course.getSections()) {
                section.setCourse(course);
                for (DaysTimesRoom daysTimesRoom : section.getDaysTimesRoomList()) {
                    daysTimesRoom.setParentSection(section);
                    daysTimesRoomList.add(daysTimesRoom);
                }
                section.clearDaysTimesRoomList();
            }
        }

        return daysTimesRoomList;
    }
    
    public void setDaysTimesRoomList(List<DaysTimesRoom> daysTimesRoomList) {
        for (DaysTimesRoom daysTimesRoom : daysTimesRoomList)
        {
            daysTimesRoom.getParentSection().addDaysTimesRoom(daysTimesRoom);
        }
    }
    
    

    public int getTotalClassEnrollment(String className) {

        int totalEnrolled = 0;

        for (Course course : courses.values()) {
            if (course.getNumber().toLowerCase().equals(className.toLowerCase())) {
                for (Section section : course.getSections()) {
                    totalEnrolled += section.getCapacity() - section.getSeatsOpen();
                }
            }
        }

        return totalEnrolled;
    }

    public List<Section> instructorHoursSections(Instructor instructor) {
        List<Section> sections = new ArrayList<Section>();

        for (Course course : courses.values()) {
            for (Section section : course.getSections()) {
                if (section.getInstructorOfRecord().getEmailAddress().equalsIgnoreCase(instructor.getEmailAddress()) && !section.getStatus().equalsIgnoreCase("canceled")) {
                    sections.add(section);
                }
            }
        }

        return sections;
    }

    public String conflictReport(List<Section> sections, boolean isRoom) {

        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < sections.size(); i++) {
            for (int j = i + 1; j < sections.size(); j++) {

                // Compare list.get(i) and list.get(j)
                List<DaysTimesRoom> listA = sections.get(i).getDaysTimesRoomList();
                List<DaysTimesRoom> listB = sections.get(j).getDaysTimesRoomList();

                for (int ii = 0; ii < listA.size(); ii++) {
                    DaysTimesRoom dtr1 = listA.get(ii);

                    for (int jj = 0; jj < listB.size(); jj++) {
                        DaysTimesRoom dtr2 = listB.get(jj);

                        if (dtr1.isHoursOverlap(dtr2) && dtr1.isDaysOverlap(dtr2)) {
                            if (isRoom) {
                                if (!dtr1.getBuilding().equalsIgnoreCase(dtr2.getBuilding())
                                        || !dtr1.getRoomNumber().equalsIgnoreCase(dtr2.getRoomNumber())) {
                                    continue;
                                }
                            }

                            // Handle course with a combined lecture.
                            // todo:tk:this technique needs to be studied
                            // further.
                            if (sections.get(i).getCourseNumber().equalsIgnoreCase(sections.get(j).getCourseNumber())
                                    && sections.get(i).getInstructorOfRecord().getEmailAddress().equalsIgnoreCase(
                                            sections.get(j).getInstructorOfRecord().getEmailAddress())
                                    && dtr1.hoursIdentical(dtr2)) {
                                continue;
                            }

                            sb.append(sections.get(i).getCourseNumber() + "-" + sections.get(i).getCourseSection()
                                    + " conflicts with " + sections.get(j).getCourseNumber() + "-"
                                    + sections.get(j).getCourseSection() + "\n");
                        }
                    }
                }

            }
        }

        return sb.toString();
    }

    public List<Section> roomUsageSections(String building, String roomNumber) {
        List<Section> sections = new ArrayList<Section>();

        for (Course course : courses.values()) {
            for (Section section : course.getSections()) {
                for (DaysTimesRoom daysTimesRoom : section.getDaysTimesRoomList()) {
                    if (daysTimesRoom.getBuilding().equalsIgnoreCase(building)
                            && (daysTimesRoom.getRoomNumber().equalsIgnoreCase(roomNumber) || roomNumber.equals("*"))) {
                        if (!sections.contains(section)) {
                            //section.setCourseNumber(course.getNumber()); //todo:tk

                            if (!section.getStatus().equalsIgnoreCase("canceled")) {
                                sections.add(section);
                            }
                        }
                    }
                }
            }
        }

        return sections;
    }

    public void differentCreditHoursReport() {
        for (Course course : courses.values()) {
            BigDecimal creditHours = new BigDecimal(-1);
            for (Section section : course.getSections()) {
                if (creditHours.intValue() == -1) {
                    creditHours = section.getCreditHours();
                }

                if (section.getCreditHours().compareTo(creditHours) != 0) {
                    System.out.println(course.getNumber());
                }
            }
        }
    }
    
    public void totalSectionMinutesReport() {
        for (Course course : courses.values()) {
            for (Section section : course.getSections()) {
                if(course.getNumber().equals("ETCO1120"))
                {
                System.out.println(course.getNumber() + "_" + section.getCourseSection()+ ": " + section.getMinutesPerSection());
                }
            }
        }
    }

    public String viewSections(List<Section> sections) {
        StringBuilder sb = new StringBuilder();

        for (Section section : sections) {

            sb.append(section.toString() + "\n");
        }

        return sb.toString();
    }

    public void setName(List<Section> sections) {
        for (Section section : sections) {
            for (DaysTimesRoom daysTimesRoom : section.getDaysTimesRoomList()) {

                daysTimesRoom.setCourseNumber(section.getCourseNumber());
                daysTimesRoom.setCourseSection(section.getCourseSection());
            }
        }

    }

    public String roomWeeklyView(List<Section> sections) {
        WeeklyView weeklyView = new WeeklyView();

        for (Section section : sections) {
            for (DaysTimesRoom daysTimesRoom : section.getDaysTimesRoomList()) {

                weeklyView.addDaysTimesRooms(daysTimesRoom);
            }
        }

        return weeklyView.getView(false);
    }

    public String roomWeeklyFilterView(List<Section> sections, String buildingName, String roomNumber) {
        WeeklyView weeklyView = new WeeklyView();

        for (Section section : sections) {
            for (DaysTimesRoom daysTimesRoom : section.getDaysTimesRoomList()) {

                if (daysTimesRoom.getBuilding().equalsIgnoreCase(buildingName)
                        && daysTimesRoom.getRoomNumber().equalsIgnoreCase(roomNumber)) {
                    weeklyView.addDaysTimesRooms(daysTimesRoom);
                }
            }
        }

        return weeklyView.getView(true);
    }

    public List<Section> roomWeeklySections(String building, String roomNumber) {
        List<Section> sections = new ArrayList<Section>();

        for (Course course : courses.values()) {
            for (Section section : course.getSections()) {
                for (DaysTimesRoom daysTimesRoom : section.getDaysTimesRoomList()) {
                    if (daysTimesRoom.getBuilding().equalsIgnoreCase(building)
                            && (daysTimesRoom.getRoomNumber().equalsIgnoreCase(roomNumber) || roomNumber.equals("*"))) {

                        daysTimesRoom.setCourseNumber(section.getCourseNumber());
                        daysTimesRoom.setCourseSection(section.getCourseSection());

                        if (!sections.contains(section)) {
                            sections.add(section);
                        }

                    }
                }
            }
        }

        return sections;
    }

    public String instructorSchedule(Instructor instructor) {
        return instructorSchedule(instructor, true);
    }

    public String instructorSchedule(Instructor instructor, boolean isConflictReport) {
        StringBuffer sb = new StringBuffer();

        sb.append("========================================================================\n");

        sb.append("INSTRUCTOR SCHEDULE FOR " + instructor.toString().trim() + "\n\n");

        List<Section> sections = instructorHoursSections(instructor);

        if (sections.size() == 0) {
            sb.append("*** NO COURSES ***\n");
        } else {
            sb.append(viewSections(sections) + "\n");

            if (isConflictReport) {
                String conflictReport = conflictReport(sections, false);

                if (!conflictReport.equals("")) {
                    sb.append("*** CONFLICT ***\n");
                    sb.append(conflictReport + "\n");
                } else {
                    setName(sections);

                    sb.append(roomWeeklyView(sections) + "\n");
                }
            }
        }

        return sb.toString();
    }
    
    public static DaysTimesRoom createDaysTimesRoom(String row) throws Exception {
        return createDaysTimesRoom(row, "", "");
    }
    
    public static DaysTimesRoom createDaysTimesRoom(String row, String building, String room) throws Exception {

        String[] daysAndTimesWithoutBothMeridians = row.split("[ -]+");

        String[] daysAndTimesWithBothMeridians = new String[5];

        daysAndTimesWithBothMeridians[0] = daysAndTimesWithoutBothMeridians[0];

        // Handle case where class spans AM and PM.
        if (daysAndTimesWithoutBothMeridians[1].endsWith("M")) {
            String timePart = daysAndTimesWithoutBothMeridians[1].substring(0,
                    daysAndTimesWithoutBothMeridians[1].length() - 2);

            String meridianPart = daysAndTimesWithoutBothMeridians[1].substring(
                    daysAndTimesWithoutBothMeridians[1].length() - 2, daysAndTimesWithoutBothMeridians[1].length());

            daysAndTimesWithBothMeridians[1] = timePart;
            daysAndTimesWithBothMeridians[2] = meridianPart;
            daysAndTimesWithBothMeridians[3] = daysAndTimesWithoutBothMeridians[2];
            daysAndTimesWithBothMeridians[4] = daysAndTimesWithoutBothMeridians[3];
        } else {
            daysAndTimesWithBothMeridians[1] = daysAndTimesWithoutBothMeridians[1];
            daysAndTimesWithBothMeridians[2] = new String(daysAndTimesWithoutBothMeridians[3]);
            daysAndTimesWithBothMeridians[3] = daysAndTimesWithoutBothMeridians[2];
            daysAndTimesWithBothMeridians[4] = daysAndTimesWithoutBothMeridians[3];
        }

        String days = daysAndTimesWithBothMeridians[0];
        String startTimeString = daysAndTimesWithBothMeridians[1];
        String startTimeMeridian = daysAndTimesWithBothMeridians[2];
        String endTimeString = daysAndTimesWithBothMeridians[3];
        String endTimeMeridian = daysAndTimesWithBothMeridians[4];

        DaysTimesRoom daysTimesRoom = new DaysTimesRoom(days, startTimeString, startTimeMeridian, endTimeString,
                endTimeMeridian, "MAIN CAMPUS", building, room);

        return daysTimesRoom;
    }
    
    

    


    public String instructorWorkload(Instructor instructor) {
        StringBuffer sb = new StringBuffer();

        List<Section> sections = instructorHoursSections(instructor);

        double totalContractHours = 0;

        if (sections.size() == 0) {
            sb.append("*** NO COURSES ***\n");
        } else {
            for (Section section : sections) {
                int lectureHours = section.getLectureHours();
                int labHours = section.getLabHours();

                double contractHours = lectureHours + .75 * labHours;

                totalContractHours += contractHours;

                sb.append(section.getCourseNumber() + ", " + contractHours);
                sb.append("\n");
            }

            sb.append("--------------\n");
            sb.append("   total " + String.format("%5.2f", totalContractHours));
        }

        return sb.toString();
    }

    public String roomSchedule(String roomName) {
        StringBuffer sb = new StringBuffer();

        sb.append("========================================================================\n");

        sb.append("ROOM SCHEDULE FOR " + roomName + "\n\n");

        String buildingName = "";
        String roomNumber = "";

        int x = 0;
        for (; x < roomName.length(); x++) {
            if (roomName.charAt(x) >= '0' && roomName.charAt(x) <= '9') {
                break;
            }
        }
        buildingName = roomName.substring(0, x);
        roomNumber = roomName.substring(x, roomName.length());

        List<Section> sections = roomUsageSections(buildingName, roomNumber);

        if (sections.size() == 0) {
            sb.append("*** NO COURSES ***\n");
        } else {

            sb.append(viewSections(sections) + "\n");

            String conflictReport = conflictReport(sections, true);

            if (!conflictReport.equals("")) {
                sb.append("*** CONFLICT REPORT ***\n");
                sb.append(conflictReport + "\n");
            } else {

                sb.append(roomWeeklyFilterView(roomWeeklySections(buildingName, roomNumber), buildingName, roomNumber)
                        + "\n");
            }
        }

        return sb.toString();
    }
    

    public void instructorScheduleReport(SemesterSchedule schedule, String fileName) {
        System.out.println();
        System.out.println("========================================================================\n");

        System.out.println("INSTRUCTOR AND ROOM SCHEDULE REPORT\n");

        System.out.println(fileName + "\n");

        System.out.println("Report run on " + new java.util.Date() + "\n");

        // String instructors = "kosan|vestich|miller,
        // a|miller|hilgarth|witherell|teeters|warfield|bratt|smith|conkel|ison|hudson|priode|penn,
        // m|holbrook|spriggs";
        // String[] instructorsArray = instructors.split("\\|");
        List<Instructor> instructorsList = schedule.getAllInstructors();

        for (Instructor instructor : instructorsList) {
            String instructorSchedule = schedule.instructorSchedule(instructor);
            System.out.println(instructorSchedule);
            //System.out.println(this.instructorWorkload(instructor));
        }

        System.out.println("\n\n************************************************************************");
        System.out.println("************************************************************************\n\n\n\n");

        // String rooms =
        // "atc101|atc103|atc105|atc110|atc160|atc164|atc170|atc180|atc201|atc203|atc204|atc205|atc206|atc263|atc301|atc303|atc304|atc305|atc306|atc309|";
        // String rooms = "atc170";
        // String[] roomsArray = rooms.split("\\|");
        List<String> roomsList = schedule.getAllRooms(false);

        for (String room : roomsList) {
            String roomSchedule = schedule.roomSchedule(room);
            System.out.println(roomSchedule);
        }
    }

    /*
	public List instructorScheduleReport(SemesterSchedule schedule, String emailAddress)
	{
List<Section> sections = instructorHoursSections(instructor);
		List day
		List<Instructor> instructorsList = schedule.getAllInstructors();

		for (Instructor instructor : instructorsList) {
			String instructorSchedule = schedule.instructorSchedule(instructor);
			System.out.println(instructorSchedule);
		}


	}
     */
    public Instructor getInstructor(SemesterSchedule schedule, String emailAddressName) throws Exception {
        emailAddressName = emailAddressName.toLowerCase().trim();

        String emailAddress = emailAddressName;

        if (!emailAddress.endsWith("@shawnee.edu")) {
            emailAddress += "@shawnee.edu";
        }

        List<Instructor> instructorsList = schedule.getAllInstructors();

        Instructor foundInstructor = null;

        for (Instructor instructor : instructorsList) {

            if (instructor.getEmailAddress().equals(emailAddress)) {
                foundInstructor = instructor;
                break;
            }
        }

        if (foundInstructor == null) {
            throw new Exception("An instructor can't be found for " + emailAddressName);
        }

        return foundInstructor;
    }

    public void instructorWorkloadReport(SemesterSchedule schedule, String fileName) {
        System.out.println();
        System.out.println("========================================================================\n");

        System.out.println("INSTRUCTOR WORKLOAD REPORT\n");

        System.out.println(fileName + "\n");

        System.out.println("Report run on " + new java.util.Date() + "\n");

        List<Instructor> instructorsList = schedule.getAllInstructors();

        for (Instructor instructor : instructorsList) {
            String instructorSchedule = schedule.instructorWorkload(instructor);

            System.out.println("========================================================================\n");
            System.out.println("INSTRUCTOR WORKLOAD FOR " + instructor.toString() + "\n");
            System.out.println(instructorSchedule);
        }

    }

    public void capacityReport() {

        System.out.print("capacities := [");

        for (Course course : courses.values()) {
            for (Section section : course.getSections()) {

                System.out.print("["
                        + section.getCapacity() + ","
                        + "\"" + section.getCourseNumber() + "\","
                        + "\"" + section.getInstructorOfRecord() + "\","
                        + "\"" + section.getDaysTimesRoomList().toString() + "\","
                        + "],");
            }
        }

        System.out.print("];");
    }

    // Enrollment for all classes in the university.
    public void totalEnrollmentReport() {

        int totalEnrollment = 0;

        for (Course course : courses.values()) {

            totalEnrollment += this.getTotalClassEnrollment(course.getNumber());
        }

        System.out.println(totalEnrollment);

    }

    class CompareBySection implements Comparator<Course> {

        public int compare(Course a, Course b) {
            return b.getSections().size() - a.getSections().size();
        }

    }

    public void sectionsReport() {

        List<Course> coursesCopy = new ArrayList();

        for (Course course : courses.values()) {
            coursesCopy.add(course);
        }

        Collections.sort(coursesCopy, new CompareBySection());

        System.out.println("Course #, Sections, Name\n");

        for (ListIterator<Course> iter = coursesCopy.listIterator(); iter.hasNext();) {
            Course course = iter.next();

            System.out.println(course.getNumber() + ", " + course.getSections().size() + ", " + course.getName());
        }
    }
    
    
    public void scheduleReport() {
        for (Course course : courses.values()) {
            System.out.println(course.getName());
            for (Section section : course.getSections()) {
                System.out.println(section.toString());
            }
        }
    }
    
    public String scheduleReportString(){
        String report = "";
        for(Course course : courses.values()){
            report += course.getName() + '\n';
            for(Section section : course.getSections()){
                report += section.toString() + "\n\n";
            }
        }
        return report;
    }


    // ========================================================
    // TO MATHPIPER.
    public String toMathPiper(String dataName) {
        //	["GEOG3350"], [["name "], ["Reg Geog:Geog of North America"], ["sections"], ["[01], [["days-and-times"], ["[[40 168 15] ] :faculty ["Dzik, Anthony J" ] :seats-open 30 :capacity 30 :open? true} } :credit-hours 3]
        StringBuilder mp = new StringBuilder();
        mp.append(dataName + "\n\n");
        mp.append("In> zz[\"HIST3170\"][\"sections\"][\"01\"][\"capacity\"]\n\n\n\n");
        mp.append("In> AssociationIndices(zz[\"HIST3170\"][\"sections\"])\n\n\n\n");
        mp.append("%mathpiper\n\n");
        mp.append("zz := [\n");

        for (Course course : courses.values()) {
            mp.append("[\"" + course.getNumber() + "\", ["); // Main map.

            // ========= Name map.
            mp.append("[\"name\", \"" + course.getName() + "\"], ");

            // ========= Sections map
            mp.append("[\"sections\", [");
            for (Section section : course.getSections()) {

                if (section.getStatus().contains("open")) {
                    mp.append("[\"" + section.getCourseSection() + "\", [");// Begin
                    // section.

                    // Days and times.
                    mp.append("[\"days-and-times\", ");
                    mp.append("[");
                    for (DaysTimesRoom daysTimesRoom : section.getDaysTimesRoomList()) {
                        mp.append("[");

                        mp.append(daysTimesRoom.getDaysPattern() + ", ");

                        mp.append(daysTimesRoom.getStartSlot() + ", ");

                        mp.append(daysTimesRoom.getSlotsLength() + ", ");

                        // Start date.
                        mp.append("[");
                        LocalDate startDate = section.getStartDate();
                        mp.append("" + startDate.getMonthValue() + ", ");
                        mp.append("" + startDate.getDayOfMonth() + ", ");
                        mp.append("" + startDate.getYear());
                        mp.append("], ");

                        // End date.
                        mp.append("[");
                        LocalDate endDate = section.getEndDate();
                        mp.append("" + endDate.getMonthValue() + ", ");
                        mp.append("" + endDate.getDayOfMonth() + ", ");
                        mp.append("" + endDate.getYear());
                        mp.append("]");

                        mp.append("], ");
                    }
                    mp.append("],");
                    mp.append("], ");

                    // Faculty.
                    mp.append("[\"faculty\", \"");
                    String allFaculty = section.getInstructors();
                    for (String faculty : allFaculty.split("\n")) {
                        mp.append(faculty.trim().replace("\"", "") + "; ");
                    }
                    mp.append("\"], ");

                    // Seats open
                    mp.append("[\"seats-open\", ");
                    mp.append(section.getSeatsOpen() + "], ");

                    // Capacity.
                    mp.append("[\"capacity\", ");
                    mp.append(section.getCapacity() + "], ");

                    // Open?
                    mp.append("[\"open?\", ");
                    mp.append((section.getStatus().contains("open") ? "True" : "False"));

                    // Credit hours.
                    mp.append("[\"credit-hours\", ");
                    mp.append(section.getCreditHours() + "], ");

                    mp.append("], ");
                    mp.append("], ");
                    mp.append("], ");
                }

            } // end for.

            mp.append("], ");// end section.

            mp.append("], ");

            mp.append("],\n");// end course.

            mp.append("], ");

        } // end courses.

        mp.append("];\n\n"); // Main map.

        mp.append("True;\n");

        mp.append("\n%/mathpiper"); // Main map.

        return mp.toString();
    }// end method.

    // TO CLOJURE MAPS.
    public String toClojureMaps(String dataName) {

        StringBuilder mp = new StringBuilder();
        //mp.append("(ns org.mathpiper.studentschedule.ssu_semester_schedule_map)\n\n");
        mp.append("(ns org.mathpiper.studentschedule.student_schedules_api)\n\n");
        mp.append("(def ssu-schedule-filename \"" + dataName + "\")\n\n");
        mp.append("(def zz2 {\n");

        for (Course course : courses.values()) {

            mp.append(":" + course.getNumber() + " {"); // Main map.

            // ========= Name map.
            mp.append(":name \"" + course.getName() + "\" ");

            // ========= Sections map
            mp.append(":sections {");
            for (Section section : course.getSections()) {

                if (section.getStatus().contains("open")) {

                    mp.append(":" + section.getCourseSection() + " {");// Begin
                    // section.

                    // Days and times.
                    mp.append(":days-and-times ");
                    mp.append("[");
                    for (DaysTimesRoom daysTimesRoom : section.getDaysTimesRoomList()) {
                        mp.append("[");

                        mp.append(daysTimesRoom.getDaysPattern() + " ");

                        mp.append(daysTimesRoom.getStartSlot() + " ");

                        mp.append(daysTimesRoom.getSlotsLength() + " ");

                        // Start date.
                        mp.append("[");
                        LocalDate startDate = section.getStartDate();
                        mp.append("" + startDate.getMonthValue() + ", ");
                        mp.append("" + startDate.getDayOfMonth() + ", ");
                        mp.append("" + startDate.getYear());
                        mp.append("] ");

                        // End date.
                        mp.append("[");
                        LocalDate endDate = section.getEndDate();
                        mp.append("" + endDate.getMonthValue() + ", ");
                        mp.append("" + endDate.getDayOfMonth() + ", ");
                        mp.append("" + endDate.getYear());
                        mp.append("]");

                        mp.append("] ");
                    }
                    mp.append("] ");
                    // mp.append("} ");

                    // Faculty.
                    mp.append(":faculty [");
                    String allFaculty = section.getInstructors();
                    if (allFaculty != null) {
                        for (String faculty : allFaculty.split("\n")) {
                            mp.append("\"" + faculty.trim() + "\" ");
                        }
                    } else {
                        mp.append("\"" + section.getInstructorOfRecord().toString().trim() + "\" ");
                    }
                    mp.append("]");

                    // Seats open.
                    mp.append(" ");
                    mp.append(":seats-open ");
                    mp.append(section.getSeatsOpen());

                    // Capacity.
                    mp.append(" ");
                    mp.append(":capacity ");
                    mp.append(section.getCapacity());

                    // Open?
                    mp.append(" ");
                    mp.append(":open? ");
                    mp.append(section.getStatus().contains("open"));

                    // Credit hours?
                    mp.append(" ");
                    mp.append(":credit-hours ");
                    mp.append(section.getCreditHours());

                    // Honors?
                    mp.append(" ");
                    mp.append(":honors? ");
                    mp.append(section.getIsHonors());

                    mp.append("} ");

                }

            } // end for.

            mp.append("}");// end section.

            mp.append("}\n");// end course.

        } // end courses.

        mp.append("} )"); // Main map.

        return mp.toString();
    }// end method.

    private String indent(int amount, boolean isNewline) {
        StringBuilder indentBuilder = new StringBuilder();

        if (isNewline) {
            indentBuilder.append("\n");
        }

        for (int index = 0; index < amount; index++) {
            indentBuilder.append("    ");
        }

        return indentBuilder.toString();
    }

    // TO XML.
    public String toXML(String dataName) {
        //	["GEOG3350"], [["name "], ["Reg Geog:Geog of North America"], ["sections"], ["[01], [["days-and-times"], ["[[40 168 15] ] :faculty ["Dzik, Anthony J" ] :seats-open 30 :capacity 30 :open? true} } :credit-hours 3]

        boolean isBlankSpaces = false;

        int indentAmount = 1;

        StringBuilder mp = new StringBuilder();
        mp.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n\n");
        mp.append("<!-- " + dataName + " v.02 -->\n\n");

        mp.append("<courses>\n");

        for (Course course : courses.values()) {

            mp.append(indent(indentAmount, isBlankSpaces) + "<course>\n");
            indentAmount++;

            mp.append(indent(indentAmount, isBlankSpaces) + "<number>\n");
            indentAmount++;
            mp.append(indent(indentAmount, false) + course.getNumber() + "\n");
            indentAmount--;
            mp.append(indent(indentAmount, false) + "</number>\n");

            mp.append(indent(indentAmount, isBlankSpaces) + "<name>\n");
            indentAmount++;
            mp.append(indent(indentAmount, false) + course.getName().replace("&", "and") + "\n");
            indentAmount--;
            mp.append(indent(indentAmount, false) + "</name>\n");

            mp.append(indent(indentAmount, isBlankSpaces) + "<sections>\n");

            for (Section section : course.getSections()) {

                indentAmount++;

                mp.append(indent(indentAmount, isBlankSpaces) + "<section" + " seats_open=\"" + section.getSeatsOpen() + "\"" + " capacity=\"" + section.getCapacity() + "\"" + " status=\"" + section.getStatus() + "\"" + " honors=\"" + section.getIsHonors() + "\"" + ">\n");// Begin
                // section.
                indentAmount++;

                // Number
                mp.append(indent(indentAmount, isBlankSpaces) + "<number>\n");
                indentAmount++;
                mp.append(indent(indentAmount, false) + section.getCourseSection() + "\n");
                indentAmount--;
                mp.append(indent(indentAmount, false) + "</number>\n");

                // Days and times.
                mp.append(indent(indentAmount, isBlankSpaces) + "<locations>\n");
                indentAmount++;

                for (DaysTimesRoom daysTimesRoom : section.getDaysTimesRoomList()) {

                    mp.append(indent(indentAmount, isBlankSpaces) + "<location>\n");
                    indentAmount++;

                    mp.append(indent(indentAmount, isBlankSpaces) + "<days>\n");
                    indentAmount++;
                    mp.append(indent(indentAmount, false) + daysTimesRoom.daysString() + "\n");
                    indentAmount--;
                    mp.append(indent(indentAmount, false) + "</days>\n");

                    // h = hour in AM/PM (1-12), m = minute in hour, a = AM/PM marker.
                    mp.append(indent(indentAmount, isBlankSpaces) + "<start_time>\n");
                    indentAmount++;
                    mp.append(indent(indentAmount, false) + daysTimesRoom.startTimeMilitaryString() + "\n");
                    indentAmount--;
                    mp.append(indent(indentAmount, false) + "</start_time>\n");

                    mp.append(indent(indentAmount, isBlankSpaces) + "<end_time>\n");
                    indentAmount++;
                    mp.append(indent(indentAmount, false) + daysTimesRoom.endTimeMilitaryString() + "\n");
                    indentAmount--;
                    mp.append(indent(indentAmount, false) + "</end_time>\n");

                    mp.append(indent(indentAmount, isBlankSpaces) + "<site>\n");
                    indentAmount++;
                    mp.append(indent(indentAmount, false) + daysTimesRoom.getSite() + "\n");
                    indentAmount--;
                    mp.append(indent(indentAmount, false) + "</site>\n");

                    mp.append(indent(indentAmount, isBlankSpaces) + "<building>\n");
                    indentAmount++;
                    mp.append(indent(indentAmount, false) + daysTimesRoom.getBuilding() + "\n");
                    indentAmount--;
                    mp.append(indent(indentAmount, false) + "</building>\n");

                    mp.append(indent(indentAmount, isBlankSpaces) + "<room_number>\n");
                    indentAmount++;
                    mp.append(indent(indentAmount, false) + daysTimesRoom.getRoomNumber() + "\n");
                    indentAmount--;
                    mp.append(indent(indentAmount, false) + "</room_number>\n");

                    indentAmount--;
                    mp.append(indent(indentAmount, isBlankSpaces) + "</location>\n");
                }
                indentAmount--;
                mp.append(indent(indentAmount, isBlankSpaces) + "</locations>\n");

                // Instructors.
                Instructor instructor = section.getInstructorOfRecord();

                mp.append(indent(indentAmount, isBlankSpaces) + "<instructors>\n");

                indentAmount++;

                mp.append(indent(indentAmount, isBlankSpaces) + "<instructor>\n");

                indentAmount++;

                mp.append(indent(indentAmount, isBlankSpaces) + "<last_name>\n");
                indentAmount++;
                mp.append(indent(indentAmount, false) + instructor.getLastName() + "\n");
                indentAmount--;
                mp.append(indent(indentAmount, false) + "</last_name>\n");

                mp.append(indent(indentAmount, isBlankSpaces) + "<first_name>\n");
                indentAmount++;
                mp.append(indent(indentAmount, false) + instructor.getFirstName() + "\n");
                indentAmount--;
                mp.append(indent(indentAmount, false) + "</first_name>\n");

                mp.append(indent(indentAmount, isBlankSpaces) + "<middle_initial>\n");
                indentAmount++;
                mp.append(indent(indentAmount, false) + instructor.getMiddleInitial() + "\n");
                indentAmount--;
                mp.append(indent(indentAmount, false) + "</middle_initial>\n");

                mp.append(indent(indentAmount, isBlankSpaces) + "<ssu_email_address>\n");
                indentAmount++;
                mp.append(indent(indentAmount, false) + instructor.getEmailAddress() + "\n");
                indentAmount--;
                mp.append(indent(indentAmount, false) + "</ssu_email_address>\n");

                indentAmount--;

                mp.append(indent(indentAmount, isBlankSpaces) + "</instructor>\n");
                indentAmount--;

                mp.append(indent(indentAmount, isBlankSpaces) + "</instructors>\n");

                // Credit hours.
                mp.append(indent(indentAmount, isBlankSpaces) + "<credit_hours>\n");
                indentAmount++;
                mp.append(indent(indentAmount, false) + section.getCreditHours() + "\n");
                indentAmount--;
                mp.append(indent(indentAmount, false) + "</credit_hours>\n");
                
                // Start date.
                mp.append(indent(indentAmount, isBlankSpaces) + "<start_date>\n");
                indentAmount++;
                LocalDate startDate = section.getStartDate();
                mp.append(indent(indentAmount, false) + startDate.getMonthValue() + "/");
                mp.append("" + startDate.getDayOfMonth() + "/");
                mp.append("" + startDate.getYear() + "\n");
                indentAmount--;
                mp.append(indent(indentAmount, false) + "</start_date>\n");

                // End date.
                mp.append(indent(indentAmount, isBlankSpaces) + "<end_date>\n");
                indentAmount++;
                LocalDate endDate = section.getEndDate();
                mp.append(indent(indentAmount, false) + endDate.getMonthValue() + "/");
                mp.append("" + endDate.getDayOfMonth() + "/");
                mp.append("" + endDate.getYear() + "\n");
                indentAmount--;
                mp.append(indent(indentAmount, false) + "</end_date>\n");

                indentAmount--;
                mp.append(indent(indentAmount, isBlankSpaces) + "</section>\n");

                indentAmount--;

            } // end for.

            mp.append(indent(indentAmount, isBlankSpaces) + "</sections>\n");

            // Requisites
            mp.append(indent(indentAmount, isBlankSpaces) + "<requisites>\n");
            indentAmount++;

            // Prerequisites.  
            mp.append(indent(indentAmount, isBlankSpaces) + "<prerequisites>\n");
            indentAmount++;
            mp.append(indent(indentAmount, isBlankSpaces) + "<course_number>\n");
            //indentAmount++;
            //mp.append(indent(indentAmount, false) + "XXXXNNNN" + "\n");
            //indentAmount--;
            mp.append(indent(indentAmount, isBlankSpaces) + "</course_number>\n");
            indentAmount--;
            mp.append(indent(indentAmount, isBlankSpaces) + "</prerequisites>\n");

            // Corequisites.  
            mp.append(indent(indentAmount, isBlankSpaces) + "<corequisites>\n");
            indentAmount++;
            mp.append(indent(indentAmount, isBlankSpaces) + "<course_number>\n");
            //indentAmount++;
            //mp.append(indent(indentAmount, false) + "XXXXNNNN" + "\n");
            //indentAmount--;
            mp.append(indent(indentAmount, isBlankSpaces) + "</course_number>\n");
            indentAmount--;
            mp.append(indent(indentAmount, isBlankSpaces) + "</corequisites>\n");
            indentAmount--;

            mp.append(indent(indentAmount, isBlankSpaces) + "</requisites>\n");
            indentAmount--;

            mp.append(indent(indentAmount, isBlankSpaces) + "</course>\n");

        } // end courses.

        mp.append("\n</courses>"); // Main map.

        return mp.toString();
    }// end method.
    
    @Override
    public javax.json.JsonStructure toJson() throws IOException{
        //ALL WE NEED
        //is the course code
        //and the list of sections
        //so our json objects will look like this:
        //{ CORS1234 : [ 01, 02, 03 ], CORS5678 : [ 03, 51, 52 ] ... }
        
        JsonBuilderFactory jsonBuilderFactory = Json.createBuilderFactory(null);
        JsonObjectBuilder jsonBuilder = jsonBuilderFactory.createObjectBuilder();
        
        //Sort them here, that way they are sorted and we don't have to do it again.
        List<String> coursesList = new ArrayList<>(courses.keySet());
        Collections.sort(coursesList);
                
        for(String course : coursesList){
            JsonArrayBuilder jsonArrayBuilder = jsonBuilderFactory.createArrayBuilder();
            JsonArray jsonSectionArray;
            
            for(Section section : courses.get(course).getSections()){
                jsonArrayBuilder.add(section.getCourseSection());
            }
            
            jsonSectionArray = jsonArrayBuilder.build();
            jsonBuilder.add(course, jsonSectionArray);
            
            
        }
        
        return jsonBuilder.build();
    }

    public abstract void loadSchedule(File file) throws Exception;

    public abstract int getLineNumber();

}
