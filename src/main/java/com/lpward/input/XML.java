/*
Copyright (c) 2016 Julius T. Kosan

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/
package com.lpward.input;

import com.lpward.domain.DaysTimesRoom;
import com.lpward.domain.StudentSchedule;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.math.BigDecimal;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Map;
import java.util.Stack;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import com.lpward.domain.course.Course;
import com.lpward.domain.course.Instructor;
import com.lpward.domain.course.Section;
import org.optaplanner.core.api.solver.Solver;
import org.optaplanner.core.api.solver.SolverFactory;

import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.Locator;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class XML extends SemesterSchedule {

    private XMLHandler handler = new XMLHandler();
    
    
    public XML()
    {
        super();
    }
    
    public XML(Map<String, Course> courses)
    {
        this.courses = courses;
    }
    
    
    public void parse(String filename) throws Exception {
        handler.parse(filename);
    }

    public static void main(String[] argv) {
        
        XML xmlReader = new XML();
         
        String filename = "src/main/resources/com/lpward/persistence/courses.xml";

        try {
            xmlReader.parse(filename);
            
            xmlReader.scheduleReport();
            
            xmlReader.sectionsReport();
            SolverFactory<StudentSchedule> sf = SolverFactory.createFromXmlResource("solver/studentScheduleSolverConfig.xml");
            Solver<StudentSchedule> solver = sf.buildSolver();
            
            StudentSchedule jimmy = new StudentSchedule();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    public void createClojureMap(String filename, String dateString) throws Exception {
        String output = toClojureMaps(dateString);

        BufferedWriter writer = null;
        try {
            writer = new BufferedWriter(
                    new FileWriter(filename));

            //new FileWriter("/home/tkosan/ssu2/cindy_schedule/tsv/fall_2016_created_on_june_11th.mpwnl"));
            //new FileWriter("/home/tkosan/ssu2/cindy_schedule/tsv/fall_2016_created_on_june_22nd.xml"));
            writer.write(output);

        } catch (IOException e) {
        } finally {
            try {
                if (writer != null) {
                    writer.close();
                }
            } catch (IOException e) {
                throw e;
            }
        }
    }

    @Override
    public void loadSchedule(File file) throws Exception {
        // TODO Auto-generated method stub

    }

    @Override
    public int getLineNumber() {
        // TODO Auto-generated method stub
        return 0;
    }

    public class XMLHandler extends DefaultHandler {

        private String startTimeTemp;

        private Stack elementStack = new Stack();

        private Stack objectStack = new Stack();

        private Locator locator;

        private SimpleDateFormat dateParser = new SimpleDateFormat("MM/dd/yyyy");

        private Writer out;

        private StringBuffer textBuffer = new StringBuffer();

        public void setDocumentLocator(Locator locator) {
            this.locator = locator;
        }

        // SAX DocumentHandler methods
        public void startDocument() throws SAXException {
        }

        public void endDocument() throws SAXException {
        }

        public void startElement(String namespaceURI, String sName, // simple name
                String qName, // qualified name
                Attributes attributes) throws SAXException {

            //Push it in element stack
            this.elementStack.push(qName);

            if ("course".equalsIgnoreCase(qName)) {
                Course course = new Course();

                this.objectStack.push(course);
            } else if ("section".equalsIgnoreCase(qName)) {

                Section section = new Section();

                if (attributes != null) {
                    int seatsOpen = Integer.parseInt(attributes.getValue("seats_open"));
                    section.setSeatsOpen(seatsOpen);

                    int capacity = Integer.parseInt(attributes.getValue("capacity"));
                    section.setCapacity(capacity);

                    String status = attributes.getValue("status");
                    section.setStatus(status);

                    String honorsStatus = attributes.getValue("honors");
                    section.setIsHonors("true".equalsIgnoreCase(honorsStatus));
                }

                this.objectStack.push(section);
            } else if ("location".equalsIgnoreCase(qName)) {
                DaysTimesRoom daysTimesRoom = new DaysTimesRoom();

                this.objectStack.push(daysTimesRoom);
            } else if ("instructor".equalsIgnoreCase(qName)) {
                Instructor instructorOfRecord = new Instructor();

                this.objectStack.push(instructorOfRecord);
            }

        }

        public void endElement(String namespaceURI, String sName, // simple name
                String qName // qualified name
        ) throws SAXException {

            String value = textBuffer.toString();

            if (this.objectStack.size() != 0) {
                processEndTags(value, qName);
            }

            this.elementStack.pop();
            textBuffer.delete(0, textBuffer.length());

        }

        private void processEndTags(String value, String qName) throws SAXException {
            
            value = value.trim();
            
            Object object = this.objectStack.peek();

            if (object instanceof Course) {
                if ("number".equals(currentElement())) {
                    Course course = (Course) object;
                    course.setNumber(value);
                } else if ("name".equals(currentElement())) {
                    Course course = (Course) object;
                    course.setName(value);
                }
            } else if (object instanceof Section) {
                Section section = (Section) object;

                if ("number".equals(currentElement())) {
                    section.setCourseSection(value);
                } else if ("credit_hours".equals(currentElement())) {
                    section.setCreditHours(new BigDecimal(value));
                } else if ("start_date".equals(currentElement())) {
                    try {
                        LocalDate startDate = LocalDate.parse(value, dateFormatter);
                        section.setStartDate(startDate);
                    } catch (Exception e) {
                        SAXException saxException = new SAXException();
                        saxException.addSuppressed(e);
                        throw saxException;
                    }
                } else if ("end_date".equals(currentElement())) {
                    try {
                        LocalDate endDate = LocalDate.parse(value, dateFormatter);
                        section.setEndDate(endDate);
                    } catch (Exception e) {
                        SAXException saxException = new SAXException();
                        saxException.addSuppressed(e);
                        throw saxException;
                    }
                } else if ("section".equals(currentElement())) {
                    this.objectStack.pop(); // Pop the section object.

                    Course course = (Course) this.objectStack.peek();

                    if (!isBlacklistSection(course, section)) {
                        course.addSection(section);
                    }
                }
            } else if (object instanceof DaysTimesRoom) {
                DaysTimesRoom daysTimesRoom = (DaysTimesRoom) object;

                if ("days".equals(currentElement())) {
                    try {
                        daysTimesRoom.setDays(value);
                    } catch (Exception e) {
                        SAXException saxException = new SAXException();
                        saxException.addSuppressed(e);
                        throw saxException;
                    }
                } else if ("start_time".equals(currentElement())) {
                    this.startTimeTemp = value;
                } else if ("end_time".equals(currentElement())) {
                    try {
                        String startTime = this.startTimeTemp;

                        String endTime = value;

                        daysTimesRoom.setStartTimeAndDurationMilitary(startTime, endTime);
                    } catch (Exception e) {
                        SAXException saxException = new SAXException("Line number: " + locator.getLineNumber() + ", Column Number: " + locator.getColumnNumber());
                        saxException.addSuppressed(e);

                        throw saxException;
                    }
                } else if ("site".equals(currentElement())) {
                    daysTimesRoom.setSite(value);
                } else if ("building".equals(currentElement())) {
                    daysTimesRoom.setBuilding(value);
                } else if ("room_number".equals(currentElement())) {
                    daysTimesRoom.setRoomNumber(value);
                } else if ("location".equals(currentElement())) {
                    this.objectStack.pop(); // Pop the DaysTimesRoom object.

                    Section section = (Section) this.objectStack.peek();

                    section.addDaysTimesRoom((DaysTimesRoom) object);

                }
            } else if (object instanceof Instructor) {
                Instructor instructor = (Instructor) object;

                if ("last_name".equals(currentElement())) {
                    instructor.setLastName(value);
                } else if ("first_name".equals(currentElement())) {
                    instructor.setFirstName(value);
                } else if ("middle_initial".equals(currentElement())) {
                    instructor.setMiddleInitial(value);
                } else if ("school_email_address".equals(currentElement())) {
                    instructor.setEmailAddress(value);
                } else if ("instructor".equals(currentElement())) {
                    this.objectStack.pop(); // Pop the Instructor object.

                    Section section = (Section) this.objectStack.peek();

                    section.setInstructorOfRecord((Instructor) object);
                }
            }

            if ("course".equals(qName)) {
                Course course = (Course) this.objectStack.pop();

                courses.put(course.getNumber(), course);
            }

        }

        //todo:tk:tempory blacklist method.
        private String[] blacklist = {}; //{"PSYC1101-01", "PTAT2190-01", "EDUC5502-01"};

        private boolean isBlacklistSection(Course course, Section section) {
            boolean isBlacklist = false;

            for (String entry : blacklist) {
                if ((course.getNumber() + "-" + section.getCourseSection()).equalsIgnoreCase(entry)) {
                    isBlacklist = true;
                }
            }

            return isBlacklist;
        }

        public void characters(char[] buf, int offset, int len) throws SAXException {

            String sRaw = new String(buf, offset, len);

            String s = sRaw.trim();

            if (s.length() == 0) {
                return; // ignore white space
            }

            textBuffer.append(s);
        }

        private String currentElement() {
            return "" + this.elementStack.peek();
        }

        private String readFile(String path, Charset encoding) throws IOException {
            byte[] encoded = Files.readAllBytes(Paths.get(path));
            return encoding.decode(ByteBuffer.wrap(encoded)).toString();
        }

        public void parse(String filename) throws Exception {
            // Use the default (non-validating) parser
            SAXParserFactory factory = SAXParserFactory.newInstance();

            out = new OutputStreamWriter(System.out, "UTF8");

            // Parse the input
            SAXParser saxParser = factory.newSAXParser();

            String xmlString = readFile(filename, StandardCharsets.UTF_8);

            saxParser.parse(new InputSource(new ByteArrayInputStream(xmlString.getBytes("utf-8"))), this);
        }

    }
}
