

/////////////////////////////////////////////////
////THIS CLASS IS ON HOLD FOR THE TIME BEING.////
/////////////////////////////////////////////////

package com.lpward.solver;

import com.lpward.domain.StudentSchedule;
import java.util.ArrayList;
import java.util.List;
import org.optaplanner.core.api.score.Score;
import org.optaplanner.core.api.score.buildin.hardsoft.HardSoftScore;
import org.optaplanner.core.impl.phase.scope.AbstractPhaseScope;
import org.optaplanner.core.impl.phase.scope.AbstractStepScope;
import org.optaplanner.core.impl.solver.recaller.BestSolutionRecaller;
import org.optaplanner.core.impl.solver.scope.DefaultSolverScope;

/*
** The point of this class is to keep a list of each score that is better
** than scoreComparator.
*/
public class AllBestSolutionsRecaller<Solution_> extends BestSolutionRecaller<Solution_>{
    
    private List<Solution_> bestSolutionsList = new ArrayList<>(10);
    private final HardSoftScore scoreComparator = HardSoftScore.of(0, -30);
    
    @Override
    public void processWorkingSolutionDuringStep(AbstractStepScope<Solution_> stepScope) {
        AbstractPhaseScope<Solution_> phaseScope = stepScope.getPhaseScope();
        Score score = stepScope.getScore();
        DefaultSolverScope<Solution_> solverScope = phaseScope.getSolverScope();
        boolean bestScoreImproved = score.compareTo(solverScope.getBestScore()) > 0;
        stepScope.setBestScoreImproved(bestScoreImproved);
        if (bestScoreImproved) {
            phaseScope.setBestSolutionStepIndex(stepScope.getStepIndex());
            Solution_ newBestSolution = stepScope.createOrGetClonedSolution();
            updateBestSolution(solverScope, score, newBestSolution);
        } else if (assertBestScoreIsUnmodified) {
            solverScope.assertScoreFromScratch(solverScope.getBestSolution());
        }
        //if this score is better than our baseline comparator score and the schedule
        //isn't already here
        Solution_ solution = solverScope.getScoreDirector().cloneWorkingSolution();
        
        if(score.compareTo(scoreComparator) >= 0 && !bestSolutionsList.contains(
                        solution)){
            if(bestSolutionsList.isEmpty()){
                bestSolutionsList.add(solution);
            } else{
                for(int i = 0; i < bestSolutionsList.size(); ++i){
                    
                    //Don't do this. Find a better way. This is very bad. Terrible software design.
                    if(((StudentSchedule)bestSolutionsList.get(i)).getScore()
                            .compareTo(((StudentSchedule)solution).getScore()) < 0){
                        bestSolutionsList.add(i, solution);
                        break;
                    }
                    else if(i++ < 10){
                        bestSolutionsList.add(solution);
                        break;
                    }    
                }
                if(bestSolutionsList.size() >= 11){
                    bestSolutionsList.remove(10);
                }
            }
            
        }
    }
    
    @Override
    public void updateBestSolution(DefaultSolverScope<Solution_> solverScope) {
        Solution_ newBestSolution = solverScope.getScoreDirector().cloneWorkingSolution();
        Score newBestScore = solverScope.getSolutionDescriptor().getScore(newBestSolution);
        updateBestSolution(solverScope, newBestScore, newBestSolution);
    }
    
    @Override
    protected void updateBestSolution(DefaultSolverScope<Solution_> solverScope, Score bestScore,
            Solution_ bestSolution) {
        if (bestScore.isSolutionInitialized()) {
            if (!solverScope.isBestSolutionInitialized()) {
                solverScope.setStartingInitializedScore(bestScore);
            }
        }
        solverScope.setBestSolution(bestSolution);
        solverScope.setBestScore(bestScore);
        solverScope.setBestSolutionTimeMillis(System.currentTimeMillis());
        solverEventSupport.fireBestSolutionChanged(solverScope, bestSolution);
    }
        
    public List<Solution_> getBestSolutionsList(){
        return this.bestSolutionsList;
    }
}
