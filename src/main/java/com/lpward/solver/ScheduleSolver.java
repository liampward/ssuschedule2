/*
    This class is mostly irrelevant now
    due to the fact that all of it is handled
    in App or TkGenerateSchedule.

    I'm going to keep it around for 
    readClassRequests() in case I need
    to do something with random class requests
    again.
*/


package com.lpward.solver;

import com.lpward.domain.DaysTimesRoom;
import com.lpward.domain.StudentPreferences;
import com.lpward.domain.SectionAssignment;
import com.lpward.domain.StudentSchedule;
import com.lpward.domain.course.Course;
import com.lpward.domain.course.Instructor;
import com.lpward.domain.course.Section;
import com.lpward.input.SemesterSchedule;
import com.lpward.input.XML;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.json.simple.JSONArray;
import org.json.simple.parser.*;
import org.optaplanner.core.api.solver.SolverFactory;
import org.optaplanner.core.api.solver.Solver;
import org.optaplanner.core.config.SolverConfigContext;
import org.optaplanner.core.config.heuristic.policy.HeuristicConfigPolicy;
import org.optaplanner.core.config.score.director.ScoreDirectorFactoryConfig;
import org.optaplanner.core.config.solver.EnvironmentMode;
import org.optaplanner.core.config.solver.SolverConfig;
import org.optaplanner.core.config.solver.recaller.BestSolutionRecallerConfig;
import org.optaplanner.core.config.solver.termination.TerminationConfig;
import org.optaplanner.core.impl.domain.solution.descriptor.SolutionDescriptor;
import org.optaplanner.core.impl.score.director.InnerScoreDirectorFactory;
import org.optaplanner.core.impl.solver.random.RandomFactory;
import org.optaplanner.core.impl.solver.recaller.BestSolutionRecaller;
import org.optaplanner.core.impl.solver.scope.DefaultSolverScope;
import static org.apache.commons.lang3.ObjectUtils.*;
import org.optaplanner.core.impl.phase.Phase;
import org.optaplanner.core.impl.solver.DefaultSolver;
import org.optaplanner.core.impl.solver.termination.BasicPlumbingTermination;
import org.optaplanner.core.impl.solver.termination.Termination;


public class ScheduleSolver {
    
    static SolverConfig solverConfig = new SolverConfig(){
        @Override
        public <Solution_> Solver<Solution_> buildSolver(SolverConfigContext configContext){
            configContext.validate();
            EnvironmentMode environmentMode_ = determineEnvironmentMode();
            boolean daemon_ = defaultIfNull(daemon, false);

            RandomFactory randomFactory = buildRandomFactory(environmentMode_);
            Integer moveThreadCount_ = resolveMoveThreadCount();
            SolutionDescriptor<Solution_> solutionDescriptor = buildSolutionDescriptor(configContext);
            ScoreDirectorFactoryConfig scoreDirectorFactoryConfig_
                    = scoreDirectorFactoryConfig == null ? new ScoreDirectorFactoryConfig()
                    : scoreDirectorFactoryConfig;
            InnerScoreDirectorFactory<Solution_> scoreDirectorFactory = scoreDirectorFactoryConfig_.buildScoreDirectorFactory(
                    configContext, environmentMode_, solutionDescriptor);
            boolean constraintMatchEnabledPreference = environmentMode_.isAsserted();
            DefaultSolverScope<Solution_> solverScope = new DefaultSolverScope<>();
            solverScope.setScoreDirector(scoreDirectorFactory.buildScoreDirector(true, constraintMatchEnabledPreference));

            BestSolutionRecallerConfig allBestSolutionsRecallerConfig = new BestSolutionRecallerConfig(){
                @Override
                public <Solution_> BestSolutionRecaller<Solution_> buildBestSolutionRecaller(EnvironmentMode environmentMode){
                    return buildAllBestSolutionsRecaller(environmentMode);
                }

                public <Solution_> AllBestSolutionsRecaller<Solution_> buildAllBestSolutionsRecaller(EnvironmentMode environmentMode){
                    AllBestSolutionsRecaller<Solution_> bestSolutionRecaller = new AllBestSolutionsRecaller<>();
                    if (environmentMode.isNonIntrusiveFullAsserted()) {
                        bestSolutionRecaller.setAssertInitialScoreFromScratch(true);
                        bestSolutionRecaller.setAssertShadowVariablesAreNotStale(true);
                        bestSolutionRecaller.setAssertBestScoreIsUnmodified(true);
                    }
                    return bestSolutionRecaller;
                }
            };
        
            BestSolutionRecaller<Solution_> bestSolutionRecaller = allBestSolutionsRecallerConfig
                    .buildBestSolutionRecaller(environmentMode_);
            HeuristicConfigPolicy configPolicy = new HeuristicConfigPolicy(environmentMode_,
                    moveThreadCount_, moveThreadBufferSize, threadFactoryClass,
                    scoreDirectorFactory);
            TerminationConfig terminationConfig_ = getTerminationConfig() == null ? new TerminationConfig()
                    : getTerminationConfig();
            BasicPlumbingTermination basicPlumbingTermination = new BasicPlumbingTermination(daemon_);
            Termination termination = terminationConfig_.buildTermination(configPolicy, basicPlumbingTermination);
            List<Phase<Solution_>> phaseList = buildPhaseList(configPolicy, bestSolutionRecaller, termination);
            return new MultiSolutionSolver<>(environmentMode_, randomFactory,
                    bestSolutionRecaller, basicPlumbingTermination, termination, phaseList, solverScope);
        }
    };
    
    public static void main(String[] argv){
        
        XML xmlReader = new XML();
         
        String filename = "src/main/resources/com/lpward/persistence/courses.xml";
        String jsonfname = "src/main/resources/com/lpward/persistence/classrequests.json";
        String[] rqsts = {"ETGG1801", "ENGL1105", "CHEM1141", "ETCO1120", "UNIV1100"};
        try {
            xmlReader.parse(filename);
            List<StudentSchedule> studentScheduleList = readClassRequests(xmlReader, jsonfname);

            SolverFactory<StudentSchedule> sf = SolverFactory.createFromXmlResource("com/lpward/solver/studentScheduleSolverConfig.xml");
            
            solverConfig.inherit(sf.getSolverConfig());
            Solver solver = solverConfig.buildSolver(new SolverConfigContext());
            
            solver.solve(studentScheduleList.get(1));
            
            List<StudentSchedule> solutions = ((MultiSolutionSolver)solver).getBestSolutions();
            for(StudentSchedule s : solutions){
                for(SectionAssignment sa : s.getSectionAssignmentList()){
                    System.out.println(sa.getCourse().getNumber() + ": " + sa.getSection().getCourseSection());
                }
                System.out.println("SCORE: " + s.getScore().getHardScore() + " / " + s.getScore().getSoftScore());
                System.out.println("--------------------");
            }
            
            System.out.println("NUMBER OF SOLUTIONS FOUND: " + solutions.size());
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
    }
    
    static public StudentSchedule GenerateStudentSchedule(SemesterSchedule ss, String[] requests) throws Exception{
        StudentSchedule student = new StudentSchedule();
        
        ArrayList<Course> courseList = new ArrayList<>();
        ArrayList<Instructor> instructorList = new ArrayList<>();
        ArrayList<Section> sectionList = new ArrayList<>();
        ArrayList<SectionAssignment> sectionAssignmentList = new ArrayList<>();
        
        Map<String, Course> tmpCourses = ss.coursesSubset(requests);
        int i = 0;
        for(Course c : tmpCourses.values()){
            courseList.add(c);
            for(Section s : c.getSections()){
                sectionList.add(s);
                instructorList.add(s.getInstructorOfRecord());
            }
            SectionAssignment tmp = new SectionAssignment();
            //tmp.setPreferOnline(online);
            tmp.setCourse(c);
            tmp.setId(i);
            sectionAssignmentList.add(tmp);
            ++i;
        }
        
        StudentPreferences pref = new StudentPreferences();
        
        pref.getDaysTimesPreference().setDays("TR");
        pref.getDaysTimesPreference().setStartTimeAndDurationMilitary("0900", "2200");
        pref.setDaysOverTimes(false);
        pref.setIncludeOnline(true);
        pref.setIncludeHonors(true);
        pref.setIncludeClosed(false);
        
        student.setCourseList(courseList);
        student.setInstructorList(instructorList);
        student.setSectionList(sectionList);
        student.setSectionAssignmentList(sectionAssignmentList);
        student.setPreferences(pref);
        
        return student;
    }
    
    public static List<StudentSchedule> readClassRequests(SemesterSchedule ss, String jsonFile){
        List<StudentSchedule> scheduleList = new ArrayList<StudentSchedule>();
        JSONParser jp = new JSONParser();
        
        try{
            FileReader reader = new FileReader(jsonFile);
            
            Object obj = jp.parse(reader);
            
            JSONArray courseRequestsArray = (JSONArray) obj;
            
            for(int i = 0; i < courseRequestsArray.size(); ++i){
                JSONArray courseRequests = (JSONArray)courseRequestsArray.get(i);
                String[] tmp = new String[courseRequests.size()];
                
                for(int j = 0; j < courseRequests.size(); ++j){
                    String course = courseRequests.get(j).toString();
                    tmp[j] = course;
                }
                scheduleList.add(GenerateStudentSchedule(ss, tmp));
            }
        } catch(Exception e){
            e.printStackTrace();
        }
        
        return scheduleList;
    }
}
