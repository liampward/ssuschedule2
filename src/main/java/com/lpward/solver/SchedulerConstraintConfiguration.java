
package com.lpward.solver;

import org.optaplanner.core.api.domain.constraintweight.ConstraintConfiguration;
import org.optaplanner.core.api.domain.constraintweight.ConstraintWeight;
import org.optaplanner.core.api.score.buildin.hardsoft.HardSoftScore;

@ConstraintConfiguration()
public class SchedulerConstraintConfiguration {
    
    @ConstraintWeight("Section class match")
    private HardSoftScore sectionClassMatch = HardSoftScore.ofHard(10);
    
    @ConstraintWeight("No time overlap")
    private HardSoftScore noTimeOverlap = HardSoftScore.ofHard(10);
    
    //@ConstraintWeight("Scheduled over null")
    //private HardSoftScore scheduledOverNull = HardSoftScore.ofHard(7);
    
    @ConstraintWeight("Class closed")
    private HardSoftScore classClosed = HardSoftScore.ofHard(5);
    
    @ConstraintWeight("Honors excluded")
    private HardSoftScore honorsExcluded = HardSoftScore.ofHard(3);
    
    public HardSoftScore getSectionClassMatch(){
        return this.sectionClassMatch;
    }
    
    public void setSectionClassMatch(HardSoftScore nScore){
        this.sectionClassMatch = nScore;
    }
    
    public void setSectionClassMatch(int nScore){
        this.sectionClassMatch = HardSoftScore.ofHard(nScore);
    }
    
    public HardSoftScore getNoTimeOverlap(){
        return this.noTimeOverlap;
    }
    
    public void setNoTimeOverlap(HardSoftScore nScore){
        this.noTimeOverlap = nScore;
    }
    
    public void setNoTimeOverlap(int nScore){
        this.noTimeOverlap = HardSoftScore.ofHard(nScore);
    }
    /*
    Depricated: 
    We're not nulling section assignments anymore.
    public HardSoftScore getScheduledOverNull(){
        return this.scheduledOverNull;
    }
    
    public void setScheduledOverNull(HardSoftScore nScore){
        this.scheduledOverNull = nScore;
    }
    
    public void setScheduledOverNull(int nScore){
        this.scheduledOverNull = HardSoftScore.ofHard(nScore);
    }
    */
    public HardSoftScore getHonorsExcluded(){
        return this.honorsExcluded;
    }
    
    public void setHonorsExcluded(HardSoftScore nScore){
        this.honorsExcluded = nScore;
    }
    
    public void setHonorsExcluded(int nScore){
        this.honorsExcluded = HardSoftScore.ofHard(nScore);
    }
    
    public HardSoftScore getClassClosed(){
        return this.classClosed;
    }
    
    public void setClassClosed(HardSoftScore nScore){
        this.classClosed = nScore;
    }
    
    public void setClassClosed(int nScore){
        this.classClosed = HardSoftScore.ofHard(nScore);
    }
}
