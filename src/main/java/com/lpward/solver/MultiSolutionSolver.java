
package com.lpward.solver;

import java.util.List;
import org.optaplanner.core.api.score.Score;
import org.optaplanner.core.api.solver.Solver;
import org.optaplanner.core.api.solver.event.SolverEventListener;
import org.optaplanner.core.config.solver.EnvironmentMode;
import org.optaplanner.core.impl.score.director.ScoreDirectorFactory;
import org.optaplanner.core.impl.solver.DefaultSolver;
import org.optaplanner.core.impl.solver.ProblemFactChange;
import org.optaplanner.core.impl.solver.random.RandomFactory;
import org.optaplanner.core.impl.solver.recaller.BestSolutionRecaller;
import org.optaplanner.core.impl.solver.scope.DefaultSolverScope;
import org.optaplanner.core.impl.solver.termination.BasicPlumbingTermination;
import org.optaplanner.core.impl.solver.termination.Termination;

public class MultiSolutionSolver<Solution_> extends DefaultSolver{

    public MultiSolutionSolver(EnvironmentMode environmentMode, RandomFactory randomFactory, BestSolutionRecaller bestSolutionRecaller, BasicPlumbingTermination basicPlumbingTermination, Termination termination, List phaseList, DefaultSolverScope solverScope) {
        super(environmentMode, randomFactory, bestSolutionRecaller, basicPlumbingTermination, termination, phaseList, solverScope);
    }

    public List<Solution_> getBestSolutions(){
        if(bestSolutionRecaller.getClass() == AllBestSolutionsRecaller.class){
            AllBestSolutionsRecaller absr = (AllBestSolutionsRecaller)bestSolutionRecaller;
            return absr.getBestSolutionsList();
        }
        return null;
    }
    
    public void emptySolutionsList(){
        ((AllBestSolutionsRecaller)getBestSolutionRecaller()).getBestSolutionsList().clear();
    }
    
}
