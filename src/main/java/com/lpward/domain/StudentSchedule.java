//This class will serve as our planning solution
package com.lpward.domain;

import com.lpward.domain.course.Course;
import com.lpward.domain.course.Instructor;
import com.lpward.domain.course.Section;
import com.lpward.input.SemesterSchedule;
import com.lpward.solver.SchedulerConstraintConfiguration;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.json.Json;
import javax.json.JsonBuilderFactory;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import org.optaplanner.core.api.domain.constraintweight.ConstraintConfigurationProvider;
import org.optaplanner.core.api.domain.solution.PlanningEntityCollectionProperty;
import org.optaplanner.core.api.domain.solution.PlanningScore;
import org.optaplanner.core.api.domain.solution.PlanningSolution;
import org.optaplanner.core.api.domain.solution.drools.ProblemFactCollectionProperty;
import org.optaplanner.core.api.domain.solution.drools.ProblemFactProperty;
import org.optaplanner.core.api.domain.valuerange.ValueRangeProvider;
import org.optaplanner.core.api.score.buildin.hardsoft.HardSoftScore;
import org.takes.rs.RsJson.Source;

@PlanningSolution
public class StudentSchedule extends AbstractIdentifiable implements Source{
    private String name;
    
    private List<Instructor> instructorList;
    private List<Course> courseList;
    private List<Section> sectionList;
    
    private List<SectionAssignment> sectionAssignmentList;
    
    private HardSoftScore score;
    
    private StudentPreferences preferences;
    
    private List<StudentSchedule> solutionsList = new ArrayList<>();
    
    @ConstraintConfigurationProvider
    private SchedulerConstraintConfiguration constraintConfig = new SchedulerConstraintConfiguration();
    
    //Optaplanner getters/setters
    @ProblemFactCollectionProperty
    public List<Course> getCourseList(){
        return this.courseList;
    }
    
    public void setCourseList(List<Course> nCourseList){
        this.courseList = nCourseList;
    }
    
    @ProblemFactCollectionProperty
    public List<Instructor> getInstructorList(){
        return this.instructorList;
    }
    
    public void setInstructorList(List<Instructor> nInstructorList){
        this.instructorList = nInstructorList;
    }
    
    @ValueRangeProvider(id = "sectionRange")
    @ProblemFactCollectionProperty
    public List<Section> getSectionList(){
        return this.sectionList;
    }
    
    public void setSectionList(List<Section> nSectionList){
        this.sectionList = nSectionList;
    }
    
    @PlanningEntityCollectionProperty
    public List<SectionAssignment> getSectionAssignmentList(){
        return this.sectionAssignmentList;
    }
    
    public void setSectionAssignmentList(List<SectionAssignment> nSectionAssignmentList){
        this.sectionAssignmentList = nSectionAssignmentList;
    }
    
    @PlanningScore()
    public HardSoftScore getScore(){
        return this.score;
    }
    
    public void setScore(HardSoftScore nScore){
        this.score = nScore;
    }
    
    @ProblemFactProperty
    public StudentPreferences getPreferences(){
        return this.preferences;
    }
    
    public void setPreferences(StudentPreferences nDaysTimesPreference){
        this.preferences = nDaysTimesPreference;
    }
    
    public SchedulerConstraintConfiguration getConstraintConfiguration(){
        return this.constraintConfig;
    }
    
    public void setConstraintConfiguration(SchedulerConstraintConfiguration nConstraintConfig){
        this.constraintConfig = nConstraintConfig;
    }
    
    //Complex getters/setters
    public boolean getIncludeOnline(){
        return this.getPreferences().getIncludeOnline();
    }
    
    public void setIncludeOnline(boolean nIncludeOnline){
        this.getPreferences().setIncludeOnline(nIncludeOnline);
    }
    
    public boolean getIncludeHonors(){
        return this.getPreferences().getIncludeHonors();
    }
    
    public void setIncludeHonors(boolean nIncludeHonors){
        this.getPreferences().setIncludeHonors(nIncludeHonors);
    }
    
    public DaysTimesRoom getPreferredMeetings(){
        return this.preferences.getDaysTimesPreference();
    }
    
    public void setPreferredMeetings(DaysTimesRoom nPreferredMeetings){
        this.preferences.setDaysTimesPreference(nPreferredMeetings);
    }
    
    public boolean getTimesOverDays(){
        return this.preferences.getDaysOverTimes();
    }
    
    public void setTimesOverDays(boolean nTimesOverDays){
        this.preferences.setDaysOverTimes(nTimesOverDays);
    }
    
    //I don't think this is going to be useful, but it might in the future.
    public List<StudentSchedule> getSolutionList(){
        return this.solutionsList;
    }
    
    public void addSolution()throws CloneNotSupportedException{
        StudentSchedule tmp = this.clone();
        this.solutionsList.add(tmp);
    }
    
    @Override
    public boolean equals(Object other){
        if(other == this)
            return true;
        
        if(!(other instanceof StudentSchedule)){
            return false;
        }
        //Small optimization - if their scores are different we know they can't be the same.
        if(this.score.compareTo(((StudentSchedule)other).getScore()) != 0){
            return false;
        }
        for(int i = 0; i < sectionAssignmentList.size(); ++i){
            SectionAssignment sa = sectionAssignmentList.get(i);
            SectionAssignment otherSA = ((StudentSchedule)other).getSectionAssignmentList().get(i);
            if(!sa.getSection().getCourseNumber().equals(otherSA.getSection().getCourseNumber())
                || !sa.getSection().getCourseSection().equals(otherSA.getSection().getCourseSection())){
                return false;
            }
        }
        return true;
    }
    
    @Override
    public StudentSchedule clone() throws CloneNotSupportedException{
        return (StudentSchedule)super.clone();
    }
    
    //Conversion stuff
    public String scheduleToString(){
        String schedule = "";
        
        for(SectionAssignment sa : sectionAssignmentList){
            schedule += sa.getSection() + "\n";
        }
        
        return schedule;
    }
    
    static public StudentSchedule CreateStudentSchedule(SemesterSchedule ss, String[] requests,
            String startTimePref, String endTimePref, String days, boolean daysOverTimes,
            boolean includeOnline, boolean includeHonors, boolean includeClosed, 
            List<String> excludedSections) throws Exception{
        StudentSchedule student = new StudentSchedule();
        
        ArrayList<Course> courseList = new ArrayList<>();
        ArrayList<Instructor> instructorList = new ArrayList<>();
        ArrayList<Section> sectionList = new ArrayList<>();
        ArrayList<SectionAssignment> sectionAssignmentList = new ArrayList<>();
        
        List<Course> courseRequests = ss.coursesSubsetList(requests);
        int i = 0;
        for(Course c : courseRequests){
            
            courseList.add(c);
            for(Section s : c.getSections()){
                if(excludedSections.contains(s.getCourseNumber() + "-" + s.getCourseSection())){
                    continue;
                }
                sectionList.add(s);
                instructorList.add(s.getInstructorOfRecord());
            }
            SectionAssignment tmp = new SectionAssignment();
            tmp.setCourse(c);
            tmp.setId(i);
            sectionAssignmentList.add(tmp);
            ++i;
        }
        StudentPreferences pref = new StudentPreferences();
        
        pref.getDaysTimesPreference().setDays(days);
        pref.getDaysTimesPreference().setStartTimeAndDurationMilitary(startTimePref, endTimePref);
        pref.setDaysOverTimes(daysOverTimes);
        pref.setIncludeHonors(includeHonors);
        pref.setIncludeOnline(includeOnline);
        pref.setIncludeClosed(includeClosed);
        student.setCourseList(courseList);
        student.setInstructorList(instructorList);
        student.setSectionList(sectionList);
        student.setSectionAssignmentList(sectionAssignmentList);
        student.setPreferences(pref);
        
        return student;
    }

    @Override
    public javax.json.JsonStructure toJson() throws IOException {
        JsonBuilderFactory jsonBuilderFactory = Json.createBuilderFactory(null);
        JsonObjectBuilder jsonBuilder = jsonBuilderFactory.createObjectBuilder();
        
        char num = '1';
        for(SectionAssignment sa : sectionAssignmentList){
            JsonObjectBuilder sectionAssignmentJsonBuilder = jsonBuilderFactory.createObjectBuilder();
            JsonObject jsonSA = null;

            sectionAssignmentJsonBuilder.add("CourseNumber", sa.getCourse().getNumber());
            sectionAssignmentJsonBuilder.add("CourseName", sa.getCourse().getName());
            sectionAssignmentJsonBuilder.add("Section", sa.getSection().getCourseSection());
            sectionAssignmentJsonBuilder.add("Faculty", sa.getSection().getInstructorOfRecord().getFirstName()
                            + " " + sa.getSection().getInstructorOfRecord().getLastName());
            String meetingTimes = "";
            for(DaysTimesRoom dtr : sa.getSection().getDaysTimesRoomList()){
                meetingTimes += dtr.startTimeString() + "-" + dtr.endTimeString()
                        + " " + dtr.daysString();
            }
            sectionAssignmentJsonBuilder.add("DaysAndTimes", meetingTimes);
            sectionAssignmentJsonBuilder.add("StartDate", sa.getSection().getStartDate().toString());
            sectionAssignmentJsonBuilder.add("EndDate", sa.getSection().getEndDate().toString());

            jsonSA = sectionAssignmentJsonBuilder.build();
            jsonBuilder.add("course" + num, jsonSA);
            
            ++num;
        }
        
        return jsonBuilder.build();
    }
}
