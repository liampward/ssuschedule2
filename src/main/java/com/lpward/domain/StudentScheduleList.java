/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lpward.domain;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.json.Json;
import javax.json.JsonBuilderFactory;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonStructure;
import org.takes.rs.RsJson.Source;


public class StudentScheduleList implements Source {
    
    private List<StudentSchedule> studentScheduleList = new ArrayList<>();
    
    public void setStudentScheduleList(List<StudentSchedule> nStudentScheduleList){
        this.studentScheduleList = nStudentScheduleList;
    }
    
    public void add(StudentSchedule ss){
        studentScheduleList.add(ss);
    }
    public void remove(StudentSchedule ss){
        studentScheduleList.remove(ss);
    }
    
    @Override
    public JsonStructure toJson() throws IOException {
        JsonBuilderFactory jsonBuilderFactory = Json.createBuilderFactory(null);
        JsonObjectBuilder jsonBuilder = jsonBuilderFactory.createObjectBuilder();
        JsonObject jsonScheduleList;
        
        char num = '1';
        for(StudentSchedule ss : studentScheduleList){
            jsonBuilder.add("schedule" + num, ss.toJson());
            ++num;
        }
        jsonScheduleList = jsonBuilder.build();
        return jsonScheduleList;
    }
    
}
