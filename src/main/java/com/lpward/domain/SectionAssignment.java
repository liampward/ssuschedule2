/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lpward.domain;

import com.lpward.domain.course.Course;
import com.lpward.domain.course.Section;
import org.optaplanner.core.api.domain.entity.PlanningEntity;
import org.optaplanner.core.api.domain.variable.PlanningVariable;

@PlanningEntity
public class SectionAssignment extends AbstractIdentifiable{
    private Course course;
    private Section section;
    
    public Course getCourse(){
        return this.course;
    }
    
    public void setCourse(Course nCourse){
        this.course = nCourse;
    }
    
    @PlanningVariable(valueRangeProviderRefs = {"sectionRange"})//, nullable = true)
    public Section getSection(){
        return this.section;
    }
    
    public void setSection(Section nSection){
        this.section = nSection;
    }
    
    public boolean sectionTimeOverlap(SectionAssignment other){
        if(this.section == null || other.section == null || this.section == other.section)
            return false;
        
        for(DaysTimesRoom dtr : section.getDaysTimesRoomList()){
            for(DaysTimesRoom otherdtr : other.getSection().getDaysTimesRoomList()){
                if(dtr.isDaysOverlap(otherdtr) && dtr.isHoursOverlap(otherdtr)){
                    return true;
                }
            }
        }
        return false;
    }
    
    public boolean preferredTimesOverlap(DaysTimesRoom pref){
        if(this.section == null)
            return false;
        
        for(DaysTimesRoom dtr : section.getDaysTimesRoomList()){
            if(dtr.startTimeMinutes() <= pref.startTimeMinutes()
                    || dtr.endTimeMinutes() >= pref.endTimeMinutes())
                    return false;
        }
        return true;
    }
    
    public boolean preferredDaysOverlap(DaysTimesRoom pref){
        if(this.section == null)
            return false;
        
        for(DaysTimesRoom dtr : section.getDaysTimesRoomList()){
            if(pref.isDaysPreferred(dtr))
                return true;
        }
        
        return false;
    }
    
    public boolean isSectionOnline(){
        if(section == null)
            return false;
        
        for(DaysTimesRoom dtr : section.getDaysTimesRoomList()){
            return dtr.getBuilding().equals("ONLI");
        }
        return false;
    }
    
    public boolean isSectionHonors(){
        if(section == null)
            return false;
        //System.out.println(section.getIsHonors());
        return section.getIsHonors();
    }
    
    public boolean isSectionOpen(){
        if(section == null)
            return false;
        return this.section.getStatus().equals("open");
    }
}
