
package com.lpward.domain;

public class StudentPreferences {
    private DaysTimesRoom daysTimesPerefrence;
    private boolean daysOverTimes, includeOnline, includeHonors, includeClosed;
    
    public StudentPreferences(){
        this.daysOverTimes = true;
        this.daysTimesPerefrence = new DaysTimesRoom();
    }
    
    public DaysTimesRoom getDaysTimesPreference(){
        return this.daysTimesPerefrence;
    }
    
    public void setDaysTimesPreference(DaysTimesRoom nDaysTimesRoomPreference){
        this.daysTimesPerefrence = nDaysTimesRoomPreference;
    }
    
    public boolean getDaysOverTimes(){
        return this.daysOverTimes;
    }
    
    public void setDaysOverTimes(boolean nDaysOverTimes){
        this.daysOverTimes = nDaysOverTimes;
    }
    
    public boolean getIncludeOnline(){
        return this.includeOnline;
    }
    
    public void setIncludeOnline(boolean nIncludeOnline){
        this.includeOnline = nIncludeOnline;
    }
    
    public boolean getIncludeHonors(){
        return this.includeHonors;
    }
    
    public void setIncludeHonors(boolean nIncludeHonors){
        this.includeHonors = nIncludeHonors;
    }
    
    public boolean getIncludeClosed(){
        return this.includeClosed;
    }
    
    public void setIncludeClosed(boolean nIncludeClosed){
        this.includeClosed = nIncludeClosed;
    }
}
