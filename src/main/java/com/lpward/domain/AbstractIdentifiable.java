
package com.lpward.domain;

public class AbstractIdentifiable {
    private int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
