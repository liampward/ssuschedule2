document.addEventListener("DOMContentLoaded", function() {
        setUpTabs();
});
var tabIdx = 1;
var courseListJSON;

function onLoad(){
    
    loadCourseList();
    setUpInput();
}

function generate(){
    var userInput = document.getElementById("form1").elements;
    
    var rqst = new XMLHttpRequest();
    var url = "/generator/generate";
    
    for(var i = 0 ; i < userInput.length ; i++){
        var item = userInput.item(i);
        if(item.value === "Sections")
            continue;
        if(!!item.value){
            if(item.type !== "checkbox")
                url += (url.split('?')[1] ? '&':'?') + item.name + "=" + item.value;
            else if(item.name === "days"){
                if(item.checked === true)
                    url += (url.split('?')[1] ? '&':'?') + "Days" + "=" + item.value;
            } else{
                url += (url.split('?')[1] ? '&':'?') + item.name + "=" + item.checked;
            }
        }
    }
    if(!url.includes("Days=")){
        alert("Please make sure you choose preferred days!");
        return;
    } else if(!url.includes("Course")){
        alert("Make sure you've input your classes!");
        return;
    }
    
    var urlParams = new URLSearchParams(url);
    var start = urlParams.get("StartPref");
    var end = urlParams.get("EndPref");
    
    if(parseInt(start) >= parseInt(end)){
        alert("Time preference makes no sense!");
        return;
    }
    
    rqst.open("GET", url);
    rqst.send(userInput);
    
    rqst.onreadystatechange=(e)=>{
        if(rqst.readyState === 4){// DONE = 4
            if(!!rqst.responseText){
                try{
                    var scheduleList = JSON.parse(rqst.responseText);
                    
                    createScheduleTab(scheduleList);
                } catch(e){
                    console.log(e);
                    alert("Something went wrong with your input!\n" +
                        "Make sure you input courses and preferences correctly.");
                }
            }else{
                alert("Unfortunately, We could not find a schedule that works with these classes.")
            } 
        } 
    };
}

function loadCourseList(){
    var rqst = new XMLHttpRequest();
    var url = "/generator/courselist";
    
    rqst.open("GET", url);
    rqst.send();
    
    rqst.onreadystatechange=(e)=>{
        if(rqst.readyState === 4){
            if(!!rqst.responseText){
                try{
                    var courseList = JSON.parse(rqst.responseText);
                    courseListJSON = courseList;
                    
                    var data = document.getElementById("CourseData");
                    for(var course in courseList){
                        var entry = document.createElement("option");
                        entry.value = course;
                        data.appendChild(entry);
                    }
                } catch(error){
                    console.log(error);
                    alert("Could not load courses.\nPlease reload page or try again later.");
                }
            }
        }
    };
}

//This is so the course list doesn't pop up until you start typing
//Basically we remove and add the list to each element as we use them
function setUpInput(){
    var inputList = document.getElementsByClassName("courseinput");
    for(var i = 0; i < inputList.length; ++i){
        var el = inputList[i];
        el.addEventListener("click", function(e) {
            var input = e.target,
                list = input.getAttribute("list");

            if(list) {
                input.removeAttribute("list");
            }
        });

        el.addEventListener("keyup", function(e) {
            var input = e.target,
            list = input.getAttribute("list");

            if(input.value === ""){
                input.removeAttribute("list");
            }
            else if(!list) {
                input.setAttribute("list", "CourseData");
                //input.removeAttribute("CourseCodes");
            }
        });
    }
}

function showSections(requestNum){
    
    var sectionsDiv = document.getElementById("sectionsSelector" + requestNum);
    if(sectionsDiv.style.display === "block"){
        sectionsDiv.style.display = "none";
        return;
    }
    
    var courseInput = document.getElementById("Course" + requestNum);
    if(courseInput === null){
        console.log("Failure");
    }
    console.log(courseListJSON[courseInput.value]);
    
    for(var section in courseListJSON[courseInput.value]){
        var sectionBox = document.createElement("input");
        sectionBox.type = "checkbox";
        sectionBox.value = section;
        sectionsDiv.appendChild(sectionBox);
    }
    
    sectionsDiv.style.display = "block";
}

function setUpTabs(){
    // toggle active
    if( items = document.querySelectorAll('.toggle') ) {
        for( var i = 0; i < items.length; i++ ) {
            var el = items[i];
            el.onclick = function() {
                    this.classList.toggle('active');
            };
        }
    }

    // tabs navigation

    var tabs = document.querySelectorAll("#tabs > *");
    var tab_blocks = document.querySelectorAll("#tab-content > *");
    for( var i = 0; i < tabs.length; i++ ) {
        var tab = tabs[i];
        var block = tab_blocks[i];
        if(block) tab.block = block;
        tab.onclick = function() {

            // add .active to current tab
            var active = document.querySelector('#tabs .active');
            if ( active ) {
                active.classList.remove('active');
                active.block.classList.remove('active');
            }		
            this.classList.add('active');
            this.block.classList.add('active');
        };
    }

    // click the first one
    tabs[0].onclick();
}

function createScheduleTab(scheduleList){
    var tabs = document.getElementById("tabs");
    var tab = document.createElement("div");
    var tab_content = document.createElement("section");
    tab.appendChild(document.createTextNode(tabIdx));
    tabIdx++;
    tabs.appendChild(tab);
    
    for(var schedule in scheduleList){
        //This creates the table information
        var idx = 1;
        table = document.createElement("table");
        //Set up header info
        var headerRow = document.createElement("tr");
        var headerAttrib = document.createElement("th");
        headerAttrib.appendChild(document.createTextNode("Request"));
        headerRow.appendChild(headerAttrib);
        headerAttrib = document.createElement("th");
        headerAttrib.appendChild(document.createTextNode("Course"));
        headerRow.appendChild(headerAttrib);
        headerAttrib = document.createElement("th");
        headerAttrib.appendChild(document.createTextNode("Name"));
        headerRow.appendChild(headerAttrib);
        headerAttrib = document.createElement("th");
        headerAttrib.appendChild(document.createTextNode("Section"));
        headerRow.appendChild(headerAttrib);
        headerAttrib = document.createElement("th");
        headerAttrib.appendChild(document.createTextNode("Faculty"));
        headerRow.appendChild(headerAttrib);
        headerAttrib = document.createElement("th");
        headerAttrib.appendChild(document.createTextNode("Days and Times"));
        headerRow.appendChild(headerAttrib);
        headerAttrib = document.createElement("th");
        headerAttrib.appendChild(document.createTextNode("Start Date"));
        headerRow.appendChild(headerAttrib);
        headerAttrib = document.createElement("th");
        headerAttrib.appendChild(document.createTextNode("End Date"));
        headerRow.appendChild(headerAttrib);
        
        var thead = document.createElement("thead");
        thead.appendChild(headerRow);
        table.appendChild(thead);
        
        var tbody = document.createElement("tbody");
        //Add the course information
        for(var course in scheduleList[schedule]){ 

            var row = document.createElement("tr");

            var attrib = document.createElement("th");
            var text = document.createTextNode(idx);
            attrib.appendChild(text);
            row.appendChild(attrib);

            for(var attribute in scheduleList[schedule][course]){
                attrib = document.createElement("th");
                text = document.createTextNode(scheduleList[schedule][course][attribute]);
                attrib.appendChild(text);
                row.appendChild(attrib);
            }
            ++idx;
            tbody.appendChild(row);
        }
        table.appendChild(tbody);
        tab_content.appendChild(table);
    }
    document.getElementById("tab-content").appendChild(tab_content);
    setUpTabs();
}

function resetInput(){
    var courseInputs = document.getElementsByClassName("courseinput");
    for(var i = 0; i < courseInputs.length; ++i){
        courseInputs[i].value = "";
    }
    
    var includes = document.getElementsByClassName("include");
    for(var i = 0; i < includes.length; ++i){
        includes[i].checked = false;
    }
    
    var days = document.getElementsByName("days");
    for(var i = 0; i < includes.length; ++i){
        days[i].checked = true;
    }
    
    document.getElementById("startPref").value = "0800";
    document.getElementById("endPref").value = "1200";
    document.getElementById("timesDaysPref").value = "times";
}