document.addEventListener("DOMContentLoaded", function() {


   console.log('Document ready.');
   
   
	// toggle active
	
	if( items = document.querySelectorAll('.toggle') ) {
		
		for( var i = 0; i < items.length; i++ ) {
			
			var el = items[i];
			el.onclick = function() {
				this.classList.toggle('active');
			}
			
		}
		
	}
	
	
	
	// tabs navigation

	var tabs = document.querySelectorAll("#tabs > *");
	var tab_blocks = document.querySelectorAll("#tab-content > *");
	
	for( var i = 0; i < tabs.length; i++ ) {
		
		var tab = tabs[i];
		var block = tab_blocks[i];
				
		if(block) tab.block = block;
		
		tab.onclick = function() {
			console.log(this.block);
			
			// add .active to current tab
			var active = document.querySelector('#tabs .active');
			if ( active ) {
				active.classList.remove('active');
				active.block.classList.remove('active');
			}		
			this.classList.add('active');
			this.block.classList.add('active');

		}
	}
	
	// click the first one
	tabs[0].onclick();


});
